module roll_holder(high_friction=false) {
    ww = 40;
    hh = 30;
    tt = 4.2;

    sy1 = hh - 10;
    sy2 = hh + 5;

    intersection() {
        yzx()
        linear_extrude(ww) polygon([
            [0, 0],
            [0, tt],
            [sy1, tt],
            [sy2, 0],
        ]);

        union() {
            let(xr = 6)
            render(5)
            difference() {
                cubex(
                    x1 = 0, x2 = ww,
                    y1 = 0, y2 = sy2 + eps,
                    z1 = 0, z2 = 4.2,
                    r = 6
                );

                translate([ww, hh] / 2)
                difference() {
                    cubex(
                        xc = 0, xs = xr*2,
                        y1 = -xr - (high_friction ? 2 : 0), y2 = sy2,
                        z1 = -eps, z2 = tt + eps,
                        r = xr
                    );

                    if (high_friction)
                    rfx()
                    rz(80)
                    cubex(
                        xc = 0, xs = xr*2,
                        y1 = -10, y2 = -5,
                        z1 = -eps, z2 = tt + eps + 3,
                    );


                }

                translate([ww/2, sy2, -eps])
                linear_extrude(tt + eps2) rfx() polygon([
                    [0, 0],
                    [0, -10],
                    [xr, -10],
                    [8.75, 0],
                ]);

                ty(hh/2)
                rfx(ww/2) tx(5) {
                    cylx(-eps, tt + eps, d=3.4);
                    tz(1.2) hexnut_hole(h=tt - 1.2 + eps, d=nut_diameter_m3);
                }
            }
        }
    }
}

module roll_holder_low_friction() {
    roll_holder(false);
}

module roll_holder_high_friction() {
    roll_holder(true);
}

module roll_holder_back() {
    difference() {
        cubex(
            x1 = 0, x2 = 40,
            y1 = 0, y2 = 30,
            z1 = 0, z2 = 3,
            r = 6
        );
        rfx(20) tx(5) ty(15) {
            cylx(-eps, 5, d=3.4);
            cylx(1, 5, d=5.5);
        }
    }
}
