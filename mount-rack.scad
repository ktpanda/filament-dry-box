module mount_rack_simple() {
    wt = mount_rack_wall_thick;

    z1 = -mount_rack_base_thick;
    z2 = 0;
    z3 = z2 + mount_rack_groove_depth;
    z4  = z3 + mount_rack_lid_space;

    x1 = 0;
    x2 = 4;
    x3 = mount_rack_side_space;
    x4 = x3 + wt;

    ml = 66;

    ramp_l = 6;

    pts1 = [
        [x1, z1],
        [x1, z3],
        [x2, z3],
        [x2, z2],
        [x3, z2],
        [x3, z4],
        [x4, z4],
        [x4, z1],
    ];

    pts2 = [
        [x1 + 3, z1],
        [x1 + 3, z3 - 4],
        [x2, z3 - 4],
        [x2, z2 - 1],
        [x3 + 3, z2 - 1],
        [x3 + 3, z4],
        [x4, z4],
        [x4, z1],
    ];

    pts3 = [
        [x1 + 2, z1],
        [x1 + 2, z3 - 4],
        [x2, z3 - 4],
        [x2, z2 - 1],
        [x3, z2 - 1],
        [x3, z4],
        [x4, z4],
        [x4, z1],
    ];

    rfx() tx(container_width/2)
    difference() {
        union() {

            xzy() {
                lxpoly2(ramp_l, lpts=pts2, upts=pts1);
                tz(ramp_l)
                lxpoly(ml - 2 * ramp_l, points=pts1);
                tz(ml - ramp_l)
                lxpoly2(ramp_l, lpts=pts1, upts=pts3);

            }
        }
    }

    tz(z4)
    difference() {
        cubex(
            xc = 0, xs = container_width + 2*x4,
            y1 = 0, y2 = ml,
            z1 = 0, z2 = 8
        );
        ty(ml/2) rfx() rfy() translate([40, 26, -eps]) {
            rz(30)
            hexnut_hole(h=2.6, d=nut_diameter_m3);
            cylinder(h=8 + eps2, d=screw_diameter_m3);
        }
    }
}
