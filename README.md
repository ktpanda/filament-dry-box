Filament dry box
================

This is a remix of a project by [Werner RH][werner_rh] originally posted to Printables [here][origstl].

Improvements I have added are:

* Printable PTFE tube holders - no need to purchase pre-made ones.
* Roll adapters screw onto the holding tube.
* Roll holders have spaces for M3 nuts
* Hygrometer holders conform to the front of the container and are changed to fit the specific hygrometers I purchased [here][hygro].
* Interchangable badges to identify filament type / color
* Drilling / placement templates for front and side holes and silica box clips
* Mounting rack to hold the containers on the roof of a shelf

Components
==========

* [Food Storage Containers, 4 pack][container]
* [PTFE tubing][ptfe]
* [Hygrometers][hygro]
* [Silica gel pellets][silica]
* [M3 screws / nuts][screws]

Box parts
=========

* **drybox-roll-holder.stl**

  Inner piece of roll holder. Has space for M3 nuts.

  ![](images/drybox-roll-holder.png)

* **drybox-roll-holder-back.stl**

  Outer piece for roll holder with recess for [round M3 screws][screws].

  ![](images/drybox-roll-holder-back.png)

* **drybox-tube-threaded.stl**

  10mm x 81mm threaded tube

  ![](images/drybox-tube-threaded.png)

* **drybox-roll-adapter-##mm.stl**

  Tapered adapters that screw onto the threaded tube and hold the spool. Available in
  various diameters for different filament spools.

  ![](images/drybox-roll-adapter-54mm.png)

* **drybox-roll-adapter-inlay.stl**

  Inlay pattern for roll adapters

  ![](images/drybox-roll-adapter-inlay.png)

* **drybox-ptfe-holder.stl**

  Holder for PTFE tubing. Print 2 for each container.

  ![](images/drybox-ptfe-holder.png)

* **drybox-ptfe-holder-nut.stl**

  Inner nut for the PTFE holder.

  ![](images/drybox-ptfe-holder-nut.png)

* **drybox-ptfe-plug.stl**

  Plug that screws into the bottom PTFE holder during printing

  ![](images/drybox-ptfe-plug.png)

* **drybox-silica-clip.stl**

  Improved clip for silica boxes. Can be printed either on side or bottom. Printing on
  the bottom is quicker, but printing on the side results in improved strength and ease of
  use.

  ![](images/drybox-silica-clip.png)

* **drybox-hygro-holder.stl**

  Hygrometer holder, mounted in the bottom front of the container with hot glue.

  ![](images/drybox-hygro-holder.png)

* **drybox-mount-rack-simple.stl**

  Mounts onto the bottom of an upper shelf above the printer and holds the containers in
  place. Print with 4 perimeters for strength.

  ![](images/drybox-mount-rack-simple.png)

* **drybox-badge-\*.stl**

  Identification badges for different filament types. Intended to print with each
  filament (or at least the same color), with a contrasting color on the back. In
  Prusaslicer, you can print a bunch of badges at once by doing the following:

  * Go to Print Settings -> Output Options, turn on "Complete individual objects", and set
  "Extruder Clearance Radius" to 20.

  * Go to Printer Settings -> Custom G-code, and set "Between Objects G-code" to

    <pre>
    G0 Z2 F1000
    M600
    G0 Z0.2 F1000
    </pre>

  * Set "After Layer Change G-code" to

    <pre>
    ;AFTER_LAYER_CHANGE
    ;[layer_z]
    {if layer_z == 0.6 + layer_height}
    G0 Z2 F1000
    M600
    G0 Z[layer_z] F1000
    {endif}
    </pre>

  This will allow you to print up to 48 badges in one print, switching filaments between
  each one.

  ![](images/drybox-badge-pla.png) ![](images/drybox-badge-petg.png)
* **drybox-badge-holder.stl**

    Badge holder, mounted on front with hot glue.

  ![](images/drybox-badge-holder.png)

Tool parts
==========

* **drybox-roll-adapter-tool.stl**

  Tool for screwing roll adapters onto filament spools. Has space for two 2mm x 15mm
  magnets.

  ![](images/drybox-roll-adapter-tool.png)

* **drybox-roll-adapter-tool-magnet-cover.stl**

  Screw-in covers for magnets roll adapter tool.

  ![](images/drybox-roll-adapter-tool-magnet-cover.png)

* **drybox-silica-funnel.stl**

  Simple funnel that I use for moving silica pellets around

  ![](images/drybox-silica-funnel.png)

* **drybox-silica-clip-template.stl**

  Drilling template for silica clip screws.

  ![](images/drybox-silica-clip-template.png)

* **drybox-front-drill-template.stl**

  Drilling template for tube holes on front.

  ![](images/drybox-front-drill-template.png)

* **drybox-side-drill-template.stl**

  Drilling template for roll holder screws on each side.

  ![](images/drybox-side-drill-template.png)

* **drybox-mount-rack-drill-template.stl**

  Drilling template for mount rack.

  ![](images/drybox-mount-rack-drill-template.png)

* **drybox-badge-holder-template.stl**

  Template for placing badge holder.

  ![](images/drybox-badge-holder-template.png)


[werner_rh]: https://www.printables.com/@werner_rh_510264
[origstl]: https://www.printables.com/model/634289-filament-dry-box-cheap-and-simple
[container]: https://www.amazon.com/dp/B0CBPVPDYR "OSTBA Food Storage Containers 4-pack"
[hygro]: https://www.amazon.com/dp/B08P3QJJZL "TASOGEN 6-pack of Digital Hygrometers"
[ptfe]: https://www.amazon.com/dp/B07B8CT1YH "PTFE Teflon Tubing, 4mm outer diameter, 25ft"
[screws]: https://www.amazon.com/gp/product/B0B51BFSWZ "M3 screws"
[silica]: https://www.amazon.com/dp/B01I5Y2DG6
[openscad]: https://openscad.org/
