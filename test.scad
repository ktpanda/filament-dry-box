module corner_test_base(rad, xw=5, xl=10) {
    let(xl = rad + xl)
    difference() {
        linear_extrude(.6) {
            difference() {
                translate([-xw, -xw])
                square(xw + xl);
                square(xl);
            }

            difference() {
                square(rad);
                translate([1, 1] * rad)
                circle(r=rad);
            }
        }
        translate([-xw + 1, -xw + 1, .4])
        linear_extrude(.4, convexity=10)
        //offset(.1)
        text(str(rad), 3.5, "DejaVu Sans:style=Bold", halign="left", valign="baseline");

    }

}

module corner_test() {
    xw = 3;
    step = 0.2;
    base = 11.2;

    spc = [40, 30];

    for (i = [0 : 11]) {
        flip = i % 2;
        xx = floor(i / 2) % xw;
        yy = floor(i / 2 / xw);
        rad = base + step * i;
        translate([xx * spc.x, yy * spc.y]) {
            if (flip) {
                translate([rad + 12, rad + 5])
                rz(180)
                corner_test_base(rad);
            } else {
                corner_test_base(rad);
            }
        }
    }
}

module width_test() {
    cubex(
        x1 = 0, x2 = 50,
        yc = 0, ys = 10,
        z1 = 0, z2 = 1.4
    );
    cubex(
        x1 = -4, x2 = 00,
        yc = 0, ys = 10,
        z1 = 0, z2 = 24
    );
}
