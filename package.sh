#!/bin/bash

IFS=

date=`date +%Y-%m-%d`
mydir=`cd \`dirname $0\`; pwd`

outf=$mydir/dist/drybox-stl-$date.zip

tmpd=`mktemp -d`

mkdir -p $tmpd/src/ktpanda $tmpd/src/origstl $tmpd/src/images $tmpd/src/fonts
mkdir -p $tmpd/stl

{
    cat <<EOF
<!DOCTYPE html>
<html>
<head>
  <title>Dry box</title>
  <style type="text/css">
    body {
      font-family: sans-serif;
    }
    h1, h2, h3 {
      background-color: #f2f2f2;
      font-weight: bold;
      border-bottom: 1px solid #eee;
    }
    h2, h3 {
      margin-top: 1.5em;
    }
    pre {
      margin-left: 30px;
      margin-right: 30px;
      background-color: #f8f8f8;
      border: 1px solid #ddd;
      border-radius: 3px 3px 3px 3px;
    }
    p > code {
      background-color: #f8f8f8;
      border: 1px solid #ddd;
      border-radius: 3px 3px 3px 3px;
    }
  </style>
</head>
  <body>
EOF

    markdown README.md

cat <<EOF
  </body>
</html>
EOF
} > $tmpd/src/README.html

ls drybox-*.stl | xargs -n1 -P12 ./render-image.sh

for fil in drybox-*.stl; do
    echo "copy $fil to stl"
    cp $fil $tmpd/src
    cp $fil $tmpd/stl
done

for fil in drybox.scad drybox.json common.scad heart.svg makefile.py make genmake Makefile README.md; do
    cp $fil $tmpd/src
done

cp images/* $tmpd/src/images
cp fonts/* $tmpd/src/fonts

for fil in consts.scad screw-defs.scad shapes.scad funcs.scad partbin.scad scadutils.py build.py utils.py make.py autoindent.py; do
    cp ktpanda/$fil $tmpd/src/ktpanda/
done

for fil in silicabox-lidv3-ex.stl silicaboxv3-ex.stl; do
    cp origstl/$fil $tmpd/src/origstl/
    cp $fil $tmpd/stl
done

cp origstl/README.md $tmpd/src/origstl/

rm -rf "$outf" "$mydir/dist/stls.zip"
(cd $tmpd/src; zip "$outf" -r .)
(cd $tmpd/stl; zip "$mydir/dist/stls.zip" -r .)

rm -rf $tmpd
