

module roll_adapter_screw(fc=0, h=10) {
    d1 = roll_adapter_center_dia + fc;
    render(10)
    intersection() {
        let(b=.8, o=1.6)
        union() {
            cylx(-b, 0, d1=d1, d2=d1 + o);
            cylx(0, h - b, d=d1 + o);
            cylx(h - b, h, d1=d1 + o, d2=d1);
        }
        tz(-3)
        screw_thread([1.4, d1 + 1.6, d1], count=10, inset=2);
    }
}

module roll_adapter_base(
    hh=roll_adapter_height,
    lip_thick=roll_adapter_lip_thick,
    center_height=roll_adapter_height,
    outer_dia = roll_adapter_center_dia,
    inner_thick = 2
) {
    difference() {
        union() {
            cylx(0, hh, d=roll_dia - 3);
            cylx(1, 5, d1=roll_dia + 3, d2=roll_dia - 3);
            cylx(0, 1, d=roll_dia + 3);
            *cylx(0, lip_thick, d=min(roll_dia + 10, 67.5));
        }
        cylx(inner_thick, hh + eps, d=roll_dia - 4.8);
        for (ang = [0 : 90 : 360]) rz(ang + 45)

        let(xr=5)
        cubex(
            x1 = 7, x2 = roll_dia/2 - 3,
            yc = 0, ys = 2 * xr,
            z1 = -eps, z2 = inner_thick + eps,
            r = xr
        );
    }

    cylx(0, center_height, d=outer_dia);

    for (ang = [0, 90]) rz(ang)
    cubex(
        xc = 0, xs = roll_dia - 3.5,
        yc = 0, ys = 1.6,
        z1 = 0, z2 = hh
    );

}

module roll_adapter_inner_text(
    inner_thick = 2
) {
    for (i = [0 : 1])
    tz(inner_thick + 0.2 * i)
    for (ang = [0 : 90 : 360]) rz(ang)
    linear_extrude(.2) {
        translate([-2, -roll_dia/2 + 3])
        rz(90)
        offset(.1 - .2 * i, $fn=10)
        text(str(roll_dia), 5, "NanumSquareRound:style=Bold", halign="left", valign="bottom");
    }

}

module roll_adapter(hh=5, sh=10) {
    inner_thick = 3.4;

    part_color(0) difference() {
        roll_adapter_base(
            hh = hh,
            lip_thick = 1.2,
            center_height = sh,
            inner_thick = inner_thick,
            outer_dia = roll_adapter_center_dia + 2
        );


        cylx(-eps, roll_adapter_height + eps, d=roll_adapter_inner_dia);

        defn = [1.6, roll_adapter_inner_dia + 1.6, roll_adapter_inner_dia];
        tz(-eps) screw_taper(sh+eps2, defn, taper_top=-1, taper_bot=-1);
    }

    part_color(1, "#222222", pxfm=[undef, tmat(z=-eps)]) {
        roll_adapter_inner_text(inner_thick);

        // Don't include inlay if we are building composite part
        if (!is_undef($part_color))
        roll_adapter_inlay($fn = (!highres && $preview) ? 10 : 50);
    }
}

function roll_adapter_combo_pos(roll_dia) = let(
    spacing = roll_dia + 6,
    x1 = sqrt(3)/2,
) spacing * [
    [-x1, 1],
    [-x1, 0],
    if (roll_dia <= 56) [-x1, -1],
    [0, 0.5],
    [0, -0.5],
    [x1, 1],
    [x1, 0],
    if (roll_dia <= 56) [x1, -1],
];

module preview_roll_adapter() {
    if (preview_tube)
    tz(-rollholder_thick)
    color("#aaaaaa")
    tube_threaded();

    if (preview_roll_tool)
    rz(45)
    roll_adapter_tool();
}

module roll_adapter_combo() {
    part_color("brim", [0, .4, .9])
    lxt(.4) difference() {
        intersection() {
            hull() tv(roll_adapter_combo_pos(roll_dia)) circle(d=roll_dia + 50);
            ty(roll_dia <= 56 ? 10 : 30)
            squarex(xs=240, ys=210);
        }
        tv(roll_adapter_combo_pos(roll_dia)) {
            circle(d=roll_dia - 1);
            rz(polyangles(18, 180/18)) squarex(y2=roll_dia/2 + 2.2, xs=5);
        }

        ty(roll_dia + 8)
        squarex(xs=40, y2=roll_dia + 20);

    }
}

module preview_roll_adapter_combo() {
    tv(roll_adapter_combo_pos(roll_dia)) {
        roll_adapter();
    }
}

module roll_adapter_inlay() {
    inner_rad = (roll_adapter_inner_dia + 1.6) / 2;
    scale_factor = (roll_dia - inner_rad) / (50 - inner_rad);


    difference() {
        lxt(0, .4) {
            for (ang = [0 : 90 : 360])
            rz(ang)
            ty(inner_rad) {
                scale(scale_factor)
                ty(16 - inner_rad)
                scale(.4)
                translate([-35/2, -35/2]) {
                    offset(1) import("heart.svg");
                }

                ty((15.4 - inner_rad) * scale_factor)
                sx(-1) offset(.15) text(str(roll_dia), 3.5, "NanumSquareRound:style=Bold", halign="center", valign="center");

            }
        }
    }
}
module roll_adapter_tool() {
    difference() {
        union() {
            linehull([[0, 30], [0, -30]])
            cylx(-8, 0, d=24);

            cubex(
                xc = 0, yc = 0,
                xs = 50, ys = 16,
                z1 = -8, z2 = 0,
                r = 8
            );

            color("blue")
            for (ang = [0 : 90 : 360]) rz(ang) {
                linehull([[0, 12], [0, 16]]) {
                    cylx(0, 2.8, d=9.6);
                    cylx(2.8, 5.8, d=4);
                }
            }
        }

        let(d1=15.6)
        ty([30, -30]) {
            cylx(-7, eps, d=d1);
            cylx(-1 - eps, eps, d=d1 + 1.6);
            tz(-5)
            screw_taper(4, [1.6, d1 + 1.6, d1], taper_bot=1, taper_top=-1);
        }

        cylx(-10, 10, d=roll_adapter_center_dia + 2.4);
    }
}

module preview_roll_adapter_tool() {
    ty(30) {
        tz(0)  rz(120) ry(180) roll_adapter_tool_magnet_cover();
        tz(-6.8) cylx(0, 2, d=15);
    }
}

module roll_adapter_tool_magnet_cover() {
    d1 = 15.6 - .4;
    difference() {
        union() {
            cylx(0, 0.4, d=d1 + 1.6);
            tz(0.4) screw_taper(4.0, [1.6, d1 + 1.6, d1], taper_bot=-1, taper_top=1);
        }
        cubex(
            xc = 0, xs = 6,
            yc = 0, ys = 1.4,
            z1 = -eps, z2 = 3
        );
    }

}

module roll_adapter_nut() {
    center_height = 10;
    outer_dia = roll_adapter_center_dia + 2;

    difference() {
        union() {
            cylx(0, center_height, d=outer_dia);
            cubex(
                xc = 0, xs = outer_dia + 20,
                yc = 0, ys = 3,
                z1 = 0, z2 = center_height
            );
        }
        defn = [1.6, roll_adapter_inner_dia + 1.6, roll_adapter_inner_dia];
        tz(-eps) screw_taper(center_height+eps2, defn, taper_top=-1, taper_bot=-1);
    }
}
