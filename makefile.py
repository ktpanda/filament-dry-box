import math

s = source('drybox')
#s.part('complete', stl=False)
p = s.part('front-drill-template')
p = s.part('side-drill-template', out_xform='rz(90)')
#p = s.part('ptfe-nut-m6')
p = s.part('ptfe-holder')
p = s.part('ptfe-holder-nut')
p = s.part('ptfe-plug')
p = s.part('ptfe-coupler')
#p = s.part('width-test')
p = s.part('silica-box')
p = s.part('silica-box-lid')
p = s.part('silica-box-funnel', out_xform='rx(180)', colors=['', 'brim'], stl=False)
for size in (25, 36):
    p.subpart(str(size), silica_box_funnel_dia=size)

p = s.part('silica-box-funnel-bottle', colors=['', 'brim'], out_xform='rx(180)')

p = s.part('silica-box-lid-combo', colors=['', 'brim'], stl=False)
p.subpart('x1', silica_box_combo_count=1)
p.subpart('x3', silica_box_combo_count=3)

p = s.part('silica-clip', out_xform='rx(90)')
p = s.part('silica-clip-template')

p = s.part('silica-funnel', out_xform='rx(180)')


p = s.part('cookiesheet-silica-funnel', stl=False)
p = s.part('cookiesheet-silica-funnel-r', out_xform='ry(-90)')
p = s.part('cookiesheet-silica-funnel-l', out_xform='ry(90)')
p = s.part('cookiesheet-silica-funnel-slide')

p = s.part('roll-holder-low-friction')
p = s.part('roll-holder-high-friction')
p = s.part('roll-holder-back')


tube_part = s.part('tube-threaded')
tube_support_part = s.part('tube-support', stl=False)

tube_support_spacing = 13, 16.5

def roll_adapter_spacing(roll_dia):
    spacing = roll_dia + 6
    x1 = spacing * math.sqrt(3) / 2
    yield -x1, 1 * spacing
    yield -x1, 0
    yield 0, .5 * spacing
    yield 0, -.5 * spacing
    yield x1, 1 * spacing
    yield x1, 0
    if roll_dia <= 56:
        yield -x1, -1 * spacing
        yield x1, -1 * spacing

for xc, yc in [(4, 3), (4, 4), (6, 4)]:
    tube_support_part.subpart(f'{xc * yc}', tube_support_count_x = xc, tube_support_count_y = yc)
    merge_parts = []
    xs, ys = tube_support_spacing
    for yy in range(yc):
        for xx in range(xc):
            xo = xs * xx
            yo = ys * (yy + (.5 if xx & 1 else 0))
            merge_parts.append((tube_part, [xo, yo, 0]))
    mergepart(f'drybox-tube-threaded-combo-{xc * yc}.stl', merge_parts)

ra = s.part('roll-adapter', stl=False, colors=['base', 'inlay'], composite=True)
ra_brim = s.part('roll-adapter-combo', colors=['brim'], stl=False)

for dia in range(50, 81, 2):
    pt = ra.subpart(f'{dia}mm', roll_dia=dia)
    bpt = ra_brim.subpart(f'{dia}mm', roll_dia=dia)

    for cpt in list(pt.get_children())[1:]:
        merge_parts = [(cpt, [xx, yy, 0]) for xx, yy in roll_adapter_spacing(dia)]
        mergepart(f'drybox-roll-adapter-combo-{dia}mm-{cpt.name}.stl', merge_parts)


#p = s.part('roll-adapter-inlay')
p = s.part('roll-adapter-tool')
p = s.part('roll-adapter-tool-magnet-cover')

p = s.part('roll-adapter-nut')

#p = s.part('corner-test')

ra = s.part('roll-preview', stl=False) # Preview-only part

p = s.part('badge', out_xform='ry(180)', stl=False)

for txt in ['pla', 'pla+', 'petg', 'pc', 'abs', 'asa', 'tpu', 'pvb', 'hips', 'pp', 'flex', 'pa', 'nyl']:
    p.subpart(txt, badge_text=txt.upper())


p = s.part('ptfe-measurement-tool', colors=['base', 'inlay', 'brim'], stl=False, preview_xform='rx(180)')
for length in (5, 10, 15, 20, 30):
    p.subpart(str(length), ptfe_tool_length=length)
p = s.part('ptfe-measurement-tool-endcap-a')
p = s.part('ptfe-measurement-tool-endcap-b')
p = s.part('ptfe-measurement-tool-stopper')

#p = s.part('badge-pla')
#p = s.part('badge-petg', out_xform='ry(180)')

p = s.part('badge-holder')
p = s.part('badge-holder-template')

p = s.part('hygro-holder')
p = s.part('hygro-clip')

p = s.part('container-base', out_xform='rz(-90)', colors=['', 'brim'])
p = s.part('container-base-preview', stl=False)
p = s.part('container-base-test', out_xform='rz(-90)')
p = s.part('container-base-connector-a')
p = s.part('container-base-connector-b')

p = s.part('mount-rack-simple', out_xform='rx(-90)')
p = s.part('mount-rack-drill-template')

s.automain()
