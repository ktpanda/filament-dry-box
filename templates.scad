
module front_drill_template(badgeholder_only=false) {
    ww = container_width;

    tt = lip_height + 1.4;

    tx(10)
    difference() {
        union() {
            lip_grabber(container_width, tt);

            rfy() {
                p1 = [0, container_width/2 - 2];
                p4 = [170, 3];
                linehull([p1, p4])
                cylx(0, tt, d=4);
            }

            cubex(
                yc=0, ys=10,
                x1=0, x2=175,
                z1=0, z2=tt,
            );

        }
        tx([20, 170]) {
            cylx(-eps, tt + eps, d=2.8);
        }

        lip_grabber_inner(container_width, tt);
    }
}

module badge_holder_template() {
    ww = container_width;

    tt = lip_height + 1.4;

    holder_size = badge_holder_size + [.4, .4];

    holder_pos = 60;

    tx(10)
    difference() {
        union() {
            lip_grabber(container_width, tt);
            p1 = [0, container_width/2 - 2];
            p2 = [holder_pos - holder_size.y/2 - 2, holder_size.x/2 + 2];

            p3 = p2 + [holder_size.y + 2, 0];

            p4 = [30, 15];

            linehull([p1, p2, p3])
            cylx(0, tt, d=4);

            linehull([[0, 15], p4])
            cylx(0, tt, d=4);

            linehull([p2, [p2.x, -p2.y], p4, p2])
            cylx(0, tt, d=4);

            linehull([[p2.x, -p2.y], [p3.x, -p3.y]])
            cylx(0, tt, d=4);
        }
        tx([20, 170]) {
            cylx(-eps, tt + eps, d=2.8);
        }

        lip_grabber_inner(container_width, tt);
    }
}

module preview_badge_holder_template() {
    tx(70) rz(90)
    badge_holder();
}

module lip_grabber(cwidth, tt, wt1=4, wt2=4) {
    let(z1=-lip_thick - wt1, z2=wt2)
    tx(z1)
    yzx() linear_extrude(z2-z1, convexity=4) {
        tx(-cwidth/2)
        square([cwidth, tt]);
        rfx() tx(-cwidth/2) {
            tx(-5)
            square([5, tt + 16]);
            ty(tt)
            difference() {
                square(container_corner_rad);
                translate([1, 1] * container_corner_rad)
                circle(r=container_corner_rad);
            }
        }
    }
}

module lip_grabber_inner(cwidth, tt) {
    cubex(
        x1 = -lip_thick, x2 = 0,
        yc = 0, ys = cwidth + 20,
        z1 = tt - lip_height, z2 = tt + 20,
    );
}

module side_drill_template() {
    cwidth = container_length;
    tt = lip_height + 1.4;

    length = 100;

    spc = 30;
    difference() {
        union() {
            lip_grabber(cwidth, tt);

            rfy() {
                linehull([
                    [0, cwidth/2 - 10],
                    [length, 15]
                ]) cylx(0, tt, d=4);
            }
            *cubex(
                x1 = -10, x2 = 10,
                yc = 0, ys = cwidth,
                z1 = 0, z2 = tt
            );
            *rfy() ty(cwidth/2) {
                cubex(
                    x1 = 0, x2 = 10,
                    y1 = 0, ys = 3,
                    z1 = 0, z2 = tt + 16
                );

                tz(tt) {
                    let(r=container_corner_rad)
                    difference() {
                        cubex(
                            x1 = 0, xs = 10,
                            y1 = -r, y2 = 0,
                            z1 = 0, z2 = r
                        );
                        translate([0, -r, r])
                        yzx() cylx(-eps, 10+eps, r=r);
                    }

                }
                *yzx() lxpoly(10, points=[
                    [0, 0],
                    [0, 3],
                    [-3, 0],
                ]);
            }
            cubex(
                x1 = 0, x2 = length,
                yc = 0, ys = 10,
                z1 = 0, z2 = tt
            );
            tx(length)
            cubex(
                xc = 0, xs = 10,
                yc = 0, ys = spc + 10,
                z1 = 0, z2 = tt,
                r = 4
            );
        }

        lip_grabber_inner(cwidth, tt);

        tx(length) {
            rfy() ty(spc/2) {
                cylx(-eps, tt+eps, d=2.5);
            }
        }

    }
}

module preview_side_drill_template() {
    tx(30)
    rx(180)
    color("green")
    import("origstl/drilling-template-axle-11cm-ex.stl");
}

module mount_rack_drill_template() {
    x4 = mount_rack_side_space + mount_rack_wall_thick;
    difference() {
        cubex(
            xc = 0, xs = container_width + 2*x4,
            y1 = 0, y2 = 66,
            z1 = 0, z2 = 2.6
        );
        ty(66/2) rfx() rfy() translate([40, 26, -eps]) {
            cylinder(h=8 + eps2, d=screw_diameter_m3);
        }
        cubex(
            xc = 0, xs = container_width + 2*x4 - 10,
            y1 = 12, y2 = 66-12,
            z1 = -eps, z2 = 4+eps
        );
    }
}

ptfe_measurement_height = 8;
ptfe_measurement_width = 20;
ptfe_measurement_snap_w = 4.2;
ptfe_measurement_snap_yofs = ptfe_measurement_width/2 - ptfe_measurement_snap_w/2 - 1.7;

module ptfe_measurement_interface(l, inner) {
    h = ptfe_measurement_height;
    l = ptfe_measurement_width/2;

    xeps = inner ? eps : 0;

    fc = inner ? 0 : .3;
    tfc = inner ? 0 : .1;

    xl = 4.5;

    zofs = 1.0;

    b = 0.8;
    path_pts = [
        [fc/2 + b, -eps],
        [fc/2 + b, zofs],
        [fc, b + zofs],
        [fc, h - 2],
        [tfc, h - 1],
        [tfc, h + eps],
        [2*l, h + xeps],
        [2*l, -xeps],
    ];

    path = create_path([
        path_straight(2),
        path_skew(xl - 1.4, -2),
        path_straight(2),
    ]);

    *lxtd(0, 0.1) polyx(path_pts);


    render(10) intersection() {
        cubex(
            x1 = -xeps, x2 = xl + xeps,
            y1 = -l - xeps, y2 = l + xeps,
            z1 = -xeps, z2 = h + xeps
        );

        union() {
            rfy() tx(-1.0) rz(-90) tx(l - 1.8) path_extrude(path_pts, path);

            if (!inner)
            tz(h)
            rfy() ty(ptfe_measurement_snap_yofs)
            xzy() lxt(zc=0, zs= ptfe_measurement_snap_w + (inner ? .6 : -.6)) {
                let(h=1.0, w=1, fc = inner ? -.1 : 0)
                polyx([
                    [0, -h - 2.5],
                    [0, 0],
                    [w, 0],
                    [w, -h]
                ]);
            }
        }
        //rfy() ty(l - 4)
    }

    if (!inner) {
        rfy() ty(ptfe_measurement_snap_yofs) {
            cubex(yc=0, ys=ptfe_measurement_snap_w - .6, x1=-1.6, x2=0, z1=0, z2=h);
        }
    }

}

module ptfe_measurement_base(actual_length, enda=true, endb=true) {
    h = ptfe_measurement_height;
    l = ptfe_measurement_width;

    difference() {
        lxt(0, h) {
            squarex(yc=0, ys=l, x1=0, x2=actual_length);
        }

        if (enda)
        ptfe_measurement_interface(l/2, true);

        if (endb) {
            tx(actual_length)
            rfy() ty(ptfe_measurement_snap_yofs) cubex(
                x1 = -2.6, x2 = eps,
                yc = 0, ys = ptfe_measurement_snap_w,
                z1 = 1, z2=h + eps
            );
        }
    }

    if (endb)
    tx(actual_length)
    ptfe_measurement_interface(l/2, false);
}


module ptfe_measurement_tool_endcap_a() {
    ptfe_measurement_base(12, enda=true, endb=false);
}

module ptfe_measurement_tool_endcap_b() {
    ptfe_measurement_base(8, enda=false, endb=true);
}

module preview_ptfe_measurement_tool_endcap_b() {
    color("blue", 0.5) render(10) tx(8) ptfe_measurement_tool_endcap_a();
}

module ptfe_measurement_tool_stopper() {
    lxt(0, 1.16) {
        sy(2) squarex(xc=0, xs=4.2, y1=0, ys=4, r3=-1, r4=-1);
        squarex(xc=0, xs=18, y1=-6, y2=0);
    }
}

module ptfe_measurement_tool() {
    h = ptfe_measurement_height;
    l = ptfe_measurement_width;

    td = 4.8;

    actual_length = 10 * ptfe_tool_length;

    part_color(0, "#222222") {
        difference() {
            ptfe_measurement_base(actual_length);

            yzx() lxt(-eps, actual_length + eps) ty(4)
            circle(d=td);
            *intersection() {
                square([4.5, 10], center=true);
                rz(45) square(4.5, center=true);
            }

            tx(open_range(0, actual_length, stepn=5)) {
                xzy()
                let(
                    xx = 1.45/2,
                    yy = h/2,
                )
                lxt(-td/2, td/2)
                ty(yy) tx(xx)
                rfx() rfy()
                polyx([
                    [0, 0],
                    [0, yy + eps],
                    [xx + .6, yy + eps],
                    [xx + .6, yy],
                    [xx, yy - 1],

                    [xx, 0]
                ]);

            }
        }
    }

    part_color(2, "#222222") {
        tx([each half_open_range_a(8, actual_length - 20, stepn=50, base=2), actual_length - 2]) rfy() {
            ty(l/2)
            brim_tab(l=2, d=16, w=6);
        }
    }

    part_color(1, "gold") {
        lxt(-eps, .52) sy(-1) {

            tx(.7) ty(td/2 + .8)
            for (x = open_range(0, actual_length, stepn=5)) tx(x) {
                if (x % 10 == 0) {
                    sevenseg_def(x / 10, sx=2.6, sy=2.4, t=1.0) {
                        tx(-$sevenseg_size.x/2 + .5)
                        ty(.5)
                        sevenseg_num();
                    }

                } else {
                    *squarex(xc = 0, xs = 1.4, y1 = 0, ys = 3);
                }

            }

            offset(.75)
            let(y1 = -l/2, y2 = -td/2)
            tx(20)  ty((y1 + y2) / 2) rz(180) {
                squarex(yc = 0, ys = .01, x1=0, x2 = 10);
                rfy() rz(45) squarex(yc = 0, ys = .01, x1=0, x2 = 2.6);
            }

        }

    }
}

module preview_ptfe_measurement_tool() {
    rx(180) {
        tx(10.05) ry(90) rz(90) ptfe_measurement_tool_stopper();
    }
}
