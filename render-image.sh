#!/bin/bash

IFS=

inf=$1
tmpscad=.tmp-$inf.scad

outf=images/${inf%.stl}.png
tmpf=images/.tmp-${inf%.stl}.png

camera=0,0,0
camang=60,0,45
camdist=200
color="#0099ff"
fsize=4000,4000
osize=x300
xform=""

case $inf in
    drybox-tube-threaded.stl)
        camera=40,0,0
        camdist=300
        xform='rotate([0,90,0])'
        ;;
    drybox-roll-adapter-inlay.stl)
        #color="#eeeeee"
        ;;
    drybox-roll-adapter-tool-magnet-cover.stl)
        xform='rotate([0,180,0])'
        ;;
    drybox-roll-adapter-tool.stl)
        xform='rotate([0,0,90])'
        ;;
    drybox-roll-adapter-*mm.stl)
        camdist=240
        ;;
    drybox-ptfe-*.stl)
        camdist=80
        ;;
    drybox-roll-holder.stl)
        camera=20,15,0
        ;;
    drybox-roll-holder-back.stl)
        camera=20,15,0
        ;;
    drybox-badge-holder.stl)
        ;;
    drybox-badge-holder-template.stl)
        camera=40,0,0
        camdist=400
        ;;
    drybox-front-drill-template.stl)
        camera=90,0,0
        camdist=600
        ;;
    drybox-side-drill-template.stl)
        camera=0,20,0
        camdist=600
        ;;
    drybox-silica-clip.stl)
        camdist=250
        ;;
    drybox-hygro-holder.stl)
        camera=10,0,0
        camdist=250
        ;;
    drybox-silica-funnel.stl)
        camera=0,0,30
        camdist=400
        xform='rotate([0,180,0])'
        ;;
    drybox-silica-clip-template.stl)
        camera=20,0,0
        camdist=300
        ;;
    drybox-mount-rack-drill-template.stl)
        camera=40,0,0
        camdist=400
        ;;
    drybox-mount-rack-simple.stl)
        camera=40,0,0
        camdist=400
        ;;
    drybox-badge-*)
        xform='rotate([0,180,0])'
        ;;

esac

cat <<EOF > $tmpscad
$xform
color("$color")
import("$inf", convexity=100);
EOF

echo "Rendering $inf"
/usr/bin/openscad $tmpscad --preview \
         --imgsize=$fsize \
         --camera ${camera},${camang},${camdist} \
         --projection p \
         --colorscheme=Metallic \
         -o $tmpf


convert $tmpf -transparent \#aaaaff -trim -resize ${osize} -bordercolor none -border 20 -define png:exclude-chunks=date,time $outf
if [ "$2" = -p ]; then
    glshowimage $tmpf
fi
rm -f $tmpf $tmpscad
echo "Done rendering $inf"
