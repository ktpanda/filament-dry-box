#!/bin/sh
toplevel=`git rev-parse --show-toplevel`
if [ -n "$toplevel" ]; then
    cd "$toplevel"
    for repo in ktpanda kicad-models; do
        if [ -e $repo/.git ]; then
            git -C $repo fetch origin master
            git -C $repo reset --hard origin/master
        fi
    done
fi
