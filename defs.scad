
/*************************************************************
* Measurements                                              */

container_width = 89.5;
container_length = 217.5;

container_width_bot = 75.8;
container_length_bot = 204.2;

lip_thick = 3.8;
lip_height = 4.2;

hygro_width = 45.2;
hygro_height = 25.8;
hygro_thick = 16.4;

hygro_lip_size = 2.2;
hygro_lip_thick = 2.2;

//------------------------------------------------------------

/*************************************************************
* Fixed params                                              */

tube_fit_clearance = .2;

rollholder_thick = 4.2;

rolltube_length = 81;
rolltube_dia = 10;

roll_adapter_cap_thick = 1.4;

badge_size = [36, 18];
badge_holder_size = [48, 20];

mount_rack_lid_space = 36;
mount_rack_side_space = 11.5;
mount_rack_groove_depth = 12;
mount_rack_wall_thick = 10;
mount_rack_base_thick = 6;

funnel_outer_dia = 24;
funnel_wall_thick = 5;

funnel_inner_dia = funnel_outer_dia - 2*funnel_wall_thick;
funnel_tube_length = 20;
funnel_height = 50;

funnel_upper_outer_dia = 90;
funnel_upper_inner_dia = funnel_upper_outer_dia - 2*funnel_wall_thick;

funnel_handle_thick = 4;
funnel_handle_length = 25;
funnel_handle_width = 20;

cookiesheet_funnel_size = [310, 140, 80];
cookiesheet_wall_thick = 5;
cookiesheet_funnel_dia = 20;
cookiesheet_funnel_len = 30;

hygro_holder_height = 20;

hygro_clip_width = 52;
hygro_clip_height = hygro_height + 6;
hygro_clip_thick = 14.0;

hygro_yofs = 7;

silica_box_outer_size = [80, 60, 16.6];
silica_box_wall_thick = 1.2;
silica_box_bottom_thick = 1.2;
silica_box_lid_thick = 1.8;
silica_box_lid_inset = 2;

silica_box_funnel_size = [74, 60];

tube_support_spacing = [13, 16.5];

//------------------------------------------------------------

/*************************************************************
* Derived params                                             */

roll_adapter_lip_thick = (rolltube_length - roll_width) / 2 - rollholder_thick + roll_inset;

roll_adapter_height = roll_adapter_lip_thick + 10;

roll_adapter_inner_dia = rolltube_dia + 2*tube_fit_clearance;
roll_adapter_center_dia = roll_adapter_inner_dia + 3.2;

//------------------------------------------------------------
