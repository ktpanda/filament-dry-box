module chamfer_box(oa, ob, ch) {
    outer_points = [
        [oa.x+ch.x, oa.y     ], // 0, 8
        [oa.x     , oa.y+ch.y], // 1, 9

        [oa.x     , ob.y-ch.y], // 2, 10
        [oa.x+ch.x, ob.y     ], // 3, 11

        [ob.x-ch.x, ob.y     ], // 4, 12
        [ob.x     , ob.y-ch.y], // 5, 13

        [ob.x     , oa.y+ch.y], // 6, 14
        [ob.x-ch.x, oa.y     ], // 7, 15
    ];

    inner_points = [
        [oa.x+ch.x, oa.y+ch.y], // 16
        [oa.x+ch.x, ob.y-ch.y], // 17
        [ob.x-ch.x, ob.y-ch.y], // 18
        [ob.x-ch.x, oa.y+ch.y], // 19
    ];

    points = [
        each xypoints(inner_points, ob.z),
        each xypoints(inner_points, oa.z+ch.z),
        each xypoints(outer_points, oa.z),
    ];

    faces = [
        //top
        [for (x=[0:3]) x],

        // sides
        for (i = [0:3]) let(b=4, n = (i + 1) % 4) [i, i + b, n + b, n],

        // lower tri
        for (i = [0:3]) let(a = 4 + i, b = 8 + i * 2) [a, b, b + 1],

        // lower quad
        for (i = [0:3]) let(n = (i + 1) % 4, b1=4, b2 = 8, b3=9) [
            b1 + i,
            b3 + 2*i,
            b2 + 2*n,
            b1 + n,
        ],

        [for (x=[15:-1:8]) x],
    ];

    let(scl=.1)
    *union() {

        colors = [
            [1, 0, 0],
            [0, 1, 0],
            [1, 1, 0],
            [0, 0, 1],
            [1, 0, 1],
            [0, 1, 1]
        ];

        for (i = indices(points))
        let(pt = points[i])
        translate(pt) scale(.05) {
            rx(45)
            lxt(zs=1) {
                squarex(xs=1, ys=1);
                tx(2) text(str(i), 2, font="DejaVu Sans", halign="left", valign="center");
            }
        }

        for (i = indices(faces))
        let(
            face = faces[i],
            fcolor = colors[i % len(colors)],
            pts = [for (j = face) points[j]],
            vnorm = normalize(cross(pts[2] - pts[0], pts[1] - pts[0])),
        ) {
            echo(fcolor);
            color(fcolor, .25) {
                //hull() for(pt = pts) translate(pt) sphere(d=scl, $fn=8);
                linehull([each pts, pts[0]]) sphere(d=scl, $fn=8);
            }
            color(fcolor * .75)
            for(pt = pts) cylpt(pt, pt + vnorm * scl * 2, d=scl);
        }

    }
    polyhedron(points, faces);
}


module badge_bevel(fc=0, yext=0) {
    size = badge_size;
    tt = 1;

    intersection() {
        yzx() lxpoly(size.x + 2*fc, center=true, points=[
            [-size.y/2 - fc, 0],
            [-size.y/2 + tt - fc, tt],
            [size.y/2 + yext - tt + fc, tt],
            [size.y/2 + yext + fc, 0],
        ]);
        xzy() lxpoly(size.y + 2*fc + yext, center=true, points=[
            [-size.x/2 - fc, 0],
            [-size.x/2 + tt - fc, tt],
            [size.x/2 - tt + fc, tt],
            [size.x/2 + fc, 0],
        ]);
    }
    tz(tt)
    linear_extrude(.6) {
        square(size - 2 * [tt, tt], center=true);
    }

}

module badge(txt=badge_text) {
    size = badge_size;
    tt = 1;

    color("#333333")
    difference() {
        chamfer_box([-size.x/2, -size.y/2, 0], [size.x/2, size.y/2, 1.6], [1, 1, 1]);

        lxt(1, 2) {
            offset(.2, $fn = 10)
            text(txt, 10, "NanumSquareRound:style=Bold", spacing=1.0, halign="center", valign="center");
        }
    }
}

module badge_holder() {
    difference() {
        linear_extrude(2, convexity=10) {
            square(badge_holder_size, center=true);
        }
        tz(1) ty(badge_holder_size.y / 2 + eps)
        linear_extrude(2, convexity=10) {
            rfx() polygon([
                [badge_size.x/2 + 3, 0],
                [badge_size.x/2 - 1, -8],
                [badge_size.x/2 - 1, 0],

            ]);
        }

        ty(1)
        tz(1+eps)
        badge_bevel(fc=.2, yext=10);
    }
}

module preview_badge_holder() {
    *ty(1)
    tz(1 + eps2)
    color("#333333", 0.5)
    render(10) badge_petg();
}
