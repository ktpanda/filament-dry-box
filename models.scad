
module roll_preview() {
    yzx() {
        if (preview_tube)
        color("#aaaaaa")
        cylinder(h = rolltube_length, d=10, center = true);

        if (preview_roll)
        color("#884422")
        roll_model();

        echo(roll_adapter_screw/360 * 1.4);

        rz(roll_adapter_rotate)
        for (int = [false, true]) rx(int ? 180 : 0) {
            if (int ? preview_right_adapter : preview_left_adapter)
            rz(int ? roll_adapter_right_rotate + roll_adapter_screw : 0)
            tz(int ? roll_adapter_screw/360 * 1.4 : 0)
            tz(-rolltube_length/2 + rollholder_thick) {
                roll_adapter();
                tz(-eps)
                color("#333333")
                roll_adapter_inlay();
            }

        }

        if (preview_roll_holder)
        rfz() {
            translate([-20, -15, -rolltube_length/2])
            render(10)
            roll_holder();
        }
    }
}

module roll_model() {
    difference() {
        rfz() tz(-roll_width/2) cylx(0, 2, d=200);
        cylinder(h=roll_width + eps, d=roll_dia, center=true);
    }
}

module hygro_model() {
    xs = 44.67;
    ys = 25.6;
    zs = 13.74;

    difference() {
        color("#222222") render(10)
        union() {
            cubex(xc=0, xs=xs, yc=0, ys=ys, z1 = -zs, z2=0);

            let(ofs=1.42)
            frustum_base(
                lz = 0,
                lx1 = -xs/2 - ofs,
                ly1 = -ys/2 -ofs,
                lx2 = xs/2 + ofs,
                ly2 = ys/2 + ofs,

                uz = 1.8,
                ux1 = -xs/2,
                uy1 = -ys/2,
                ux2 = xs/2,
                uy2 = ys/2
            );
        }

        color("#909090")
        cubex(xc = 0, xs = 35.7, yc = 0, ys = 16.6, z1 = 0, z2 = 2, r = 1);
    }
}

module container_model() {
    cubex(
        xc = 0, xs = container_width,
        y1 = 0, y2 = container_length,
        z1 = 0, z2 = -200,
        r = container_corner_rad
    );
    cubex(
        xc = 0, xs = container_width + 2*lip_height,
        y1 = -lip_height, y2 = container_length + lip_height,
        z1 = -25, zs = lip_thick,
        r = container_corner_rad
    );
    rfx() tx(container_width/2)
    ty(container_length/2)
    rfy() ty(30) {
        cubex(
            x1=0, x2=lip_height,
            y1=0, y2=34,
            z1=-25, zs=-4
        );

    }
}
