include <ktpanda/consts.scad>
include <ktpanda/screw-defs.scad>
use <ktpanda/funcs.scad>
use <ktpanda/partbin.scad>
use <ktpanda/shapes.scad>
use <ktpanda/sevenseg.scad>

fit_clearance = 0.2;
