tube_dia = 4.25;

module ptfe_nut_m6() {
    difference() {
        hexnut_hole(d=12, h=6);
        tz(-1) render(10) screw_thread(screw_m6_int_max, count=7, $fn=80);
        cylx(-eps, 5+eps, d=4.8);
        cylx(3, 6+eps, d=8.8);
    }
}

module ptfe_holder_nut(screw=screw_m9_int_max) {
    difference() {
        hexnut_hole(d=14, h=5);
        tz(-1) _screw_thread(screw, count=7, $fn=80);
        cylx(-eps, 6+eps, d=screw[2]);
        *cylx(3, 6+eps, d=8.8);
    }
}

module ptfe_holder(screw=screw_m9_ext_min) {

    z1 = -5;
    z2 = z1 + 3;
    z3 = z2 + 3;
    zt = 6;

    difference() {
        union() {
            d1 = screw[2];
            d2 = screw[1];

            tz(z1)
            hexnut_hole(d=12, h=-z1);
            cylx(0, zt, d=d1);
            cylx(0, 1, d=d2);

            tz(1)
            screw_taper(4, screw, taper_top=1, $fn=80);
        }

        dfn = screw_m6_int_max;


        *cylx(z1-eps, zt+eps, d=filament_dia);
        *cylx(z3 - eps, z4, d1=dfn[2], d2=filament_dia);
        *cylx(z1-eps, z2+eps, d=tube_dia);

        tz(z1-eps) {
            screw_taper(z2 - z1 + eps, dfn, taper_top=1, taper_bot=-1, $fn=80);
            //cylx(0, (dfn[1] - dfn[2]) / 2, d1=dfn[1], d2=dfn[2]);
        }

        cylx(z2 - eps, z3, d1=dfn[2], d2=tube_dia);
        cylx(z3 - eps, zt+eps, d=tube_dia);
    }
}

module ptfe_plug() {
    dfn = screw_m6_ext_min;
    tz(-5)
    hexnut_hole(d=12, h=5);
    screw_taper(3, dfn, taper_top=1, taper_bot=0);

    zz = 4.9;
    cylinder(h=zz, d=dfn[2]);
    tz(zz) {
        cylx(0, 2.6, d1=dfn[2], d2=3.8);
        cylx(2.6, 5, d=3.8);
    }
}


module preview_ptfe_holder(screw=screw_m9_ext_min) {
    tz(6+eps)
    rx(180) color("green") render(6) ptfe_holder_nut(screw=screw_m9_int_max);

    *tz(-5 - eps)
    //rz(-90)
    color("red") render(6) ptfe_plug();
}

module ptfe_coupler_inner(h, center_expand) {
    d1 = tube_dia + 1.2;
    d2 = tube_dia - .25;
    d3 = tube_dia - 1;

    yofs1 = .6;
    yofs2 = 2.5;

    lathe(d=[
        [-eps, d3],
        [0, d3],
        [yofs1 + center_expand * (yofs1/(d2-d3)), d2 + center_expand],
        [h/2 - yofs2 - center_expand * (yofs2/(d2-d1)), d2 + center_expand],
        [h/2, d1],
        [h/2 + eps, d1]
    ]);
}

module ptfe_coupler() {
    h = 15;

    difference() {
        lxt(-h/2, h/2) {
            hexagon(d=9);
        }

        union() {
            rfz()
            ptfe_coupler_inner(h, 0);

            difference() {
                rfz()
                ptfe_coupler_inner(h, .5);
                lxt(-h/2 - eps, h/2 + eps)
                for (a = [-120, 0, 120]) rz(a) squarex(x1=0, xs=tube_dia/2 + 1, yc=0, ys=1);

            }
        }
    }
}
