include <common.scad>

/* [Common] */
part = "front-drill-template"; // [front-drill-template, side-drill-template, ptfe-holder, ptfe-holder-nut, ptfe-plug, ptfe-coupler, silica-box, silica-box-lid, silica-box-funnel, silica-box-funnel-bottle, silica-box-lid-combo, silica-clip, silica-clip-template, silica-funnel, cookiesheet-silica-funnel, cookiesheet-silica-funnel-r, cookiesheet-silica-funnel-l, cookiesheet-silica-funnel-slide, roll-holder-low-friction, roll-holder-high-friction, roll-holder-back, tube-threaded, tube-support, roll-adapter, roll-adapter-combo, roll-adapter-tool, roll-adapter-tool-magnet-cover, roll-adapter-nut, roll-preview, badge, ptfe-measurement-tool, ptfe-measurement-tool-endcap-a, ptfe-measurement-tool-endcap-b, ptfe-measurement-tool-stopper, badge-holder, badge-holder-template, hygro-holder, hygro-clip, container-base, container-base-preview, container-base-test, container-base-connector-a, container-base-connector-b, mount-rack-simple, mount-rack-drill-template]
highres = false;
$fn = (!highres && $preview) ? 30 : 100;

/* [Slice] */
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : .125 : 200]

/* [Preview parts] */
preview_roll_holder = true;
preview_left_adapter = true;
preview_right_adapter = true;
preview_roll = true;
preview_tube = true;
preview_silica_box = true;
preview_roll_tool = true;

preview_container = true;

path_debug = false;

roll_adapter_rotate = 0; // [0 : 360]
roll_adapter_right_rotate = 210; // [0 : 360]
roll_adapter_screw = 0; // [-720 : 720]
hygro_xofs = 0; // [-5 : .25 : 5]
hygro_zofs = 0; // [0 : .25 : 32]
hygro_yrot = 0; // [0 : .25 : 20]
lid_slide = 0; // [0 : .25 : 80]

debug_roundness = 0; // [0 : 0.02 : 1]

/* [Parameters] */

roll_dia = 54;
roll_width = 67;
roll_inset = 0;
screw_int = false;
container_corner_rad = 13.0;
badge_text = "PLA";
ptfe_tool_length = 20;
silica_box_combo_count = 3;
silica_box_funnel_dia = 40;
tube_support_count_x = 6;
tube_support_count_y = 4;

module __nc__() {} // end of customizer variables

include <defs.scad>
include <templates.scad>
include <ptfe-holder.scad>
include <silica-box.scad>
include <roll-adapter.scad>
include <roll-holder.scad>
include <roll-tube.scad>
include <mount-rack.scad>
include <badge.scad>
include <hygro-holder.scad>
include <container-base.scad>

include <models.scad>


// === automain start ===
preview_slice(axis=slice_axis, vc=slice_pos) {
    if (part == "front-drill-template") {
        if ($preview) {
            front_drill_template();
        } else {
            front_drill_template();
        }
    }
    else if (part == "side-drill-template") {
        if ($preview) {
            side_drill_template();
            preview_side_drill_template();
        } else {
            rz(90)
            side_drill_template();
        }
    }
    else if (part == "ptfe-holder") {
        if ($preview) {
            ptfe_holder();
            preview_ptfe_holder();
        } else {
            ptfe_holder();
        }
    }
    else if (part == "ptfe-holder-nut") {
        if ($preview) {
            ptfe_holder_nut();
        } else {
            ptfe_holder_nut();
        }
    }
    else if (part == "ptfe-plug") {
        if ($preview) {
            ptfe_plug();
        } else {
            ptfe_plug();
        }
    }
    else if (part == "ptfe-coupler") {
        if ($preview) {
            ptfe_coupler();
        } else {
            ptfe_coupler();
        }
    }
    else if (part == "silica-box") {
        if ($preview) {
            silica_box();
            preview_silica_box();
        } else {
            silica_box();
        }
    }
    else if (part == "silica-box-lid") {
        if ($preview) {
            silica_box_lid();
        } else {
            silica_box_lid();
        }
    }
    else if (part == "silica-box-funnel") {
        if ($preview) {
            silica_box_funnel();
            preview_silica_box_funnel();
        } else {
            rx(180)
            silica_box_funnel();
        }
    }
    else if (part == "silica-box-funnel-bottle") {
        if ($preview) {
            silica_box_funnel_bottle();
            preview_silica_box_funnel_bottle();
        } else {
            rx(180)
            silica_box_funnel_bottle();
        }
    }
    else if (part == "silica-box-lid-combo") {
        if ($preview) {
            silica_box_lid_combo();
        } else {
            silica_box_lid_combo();
        }
    }
    else if (part == "silica-clip") {
        if ($preview) {
            silica_clip();
            preview_silica_clip();
        } else {
            rx(90)
            silica_clip();
        }
    }
    else if (part == "silica-clip-template") {
        if ($preview) {
            silica_clip_template();
            preview_silica_clip_template();
        } else {
            silica_clip_template();
        }
    }
    else if (part == "silica-funnel") {
        if ($preview) {
            silica_funnel();
        } else {
            rx(180)
            silica_funnel();
        }
    }
    else if (part == "cookiesheet-silica-funnel") {
        if ($preview) {
            cookiesheet_silica_funnel();
            preview_cookiesheet_silica_funnel();
        } else {
            cookiesheet_silica_funnel();
        }
    }
    else if (part == "cookiesheet-silica-funnel-r") {
        if ($preview) {
            cookiesheet_silica_funnel_r();
        } else {
            ry(-90)
            cookiesheet_silica_funnel_r();
        }
    }
    else if (part == "cookiesheet-silica-funnel-l") {
        if ($preview) {
            cookiesheet_silica_funnel_l();
        } else {
            ry(90)
            cookiesheet_silica_funnel_l();
        }
    }
    else if (part == "cookiesheet-silica-funnel-slide") {
        if ($preview) {
            cookiesheet_silica_funnel_slide();
        } else {
            cookiesheet_silica_funnel_slide();
        }
    }
    else if (part == "roll-holder-low-friction") {
        if ($preview) {
            roll_holder_low_friction();
        } else {
            roll_holder_low_friction();
        }
    }
    else if (part == "roll-holder-high-friction") {
        if ($preview) {
            roll_holder_high_friction();
        } else {
            roll_holder_high_friction();
        }
    }
    else if (part == "roll-holder-back") {
        if ($preview) {
            roll_holder_back();
        } else {
            roll_holder_back();
        }
    }
    else if (part == "tube-threaded") {
        if ($preview) {
            tube_threaded();
            preview_tube_threaded();
        } else {
            tube_threaded();
        }
    }
    else if (part == "tube-support") {
        if ($preview) {
            tube_support();
            preview_tube_support();
        } else {
            tube_support();
        }
    }
    else if (part == "roll-adapter") {
        if ($preview) {
            roll_adapter();
            preview_roll_adapter();
        } else {
            roll_adapter();
        }
    }
    else if (part == "roll-adapter-combo") {
        if ($preview) {
            roll_adapter_combo();
            preview_roll_adapter_combo();
        } else {
            roll_adapter_combo();
        }
    }
    else if (part == "roll-adapter-tool") {
        if ($preview) {
            roll_adapter_tool();
            preview_roll_adapter_tool();
        } else {
            roll_adapter_tool();
        }
    }
    else if (part == "roll-adapter-tool-magnet-cover") {
        if ($preview) {
            roll_adapter_tool_magnet_cover();
        } else {
            roll_adapter_tool_magnet_cover();
        }
    }
    else if (part == "roll-adapter-nut") {
        if ($preview) {
            roll_adapter_nut();
        } else {
            roll_adapter_nut();
        }
    }
    else if (part == "roll-preview") {
        if ($preview) {
            roll_preview();
        } else {
            roll_preview();
        }
    }
    else if (part == "badge") {
        if ($preview) {
            badge();
        } else {
            ry(180)
            badge();
        }
    }
    else if (part == "ptfe-measurement-tool") {
        if ($preview) {
            rx(180)
            ptfe_measurement_tool();
            preview_ptfe_measurement_tool();
        } else {
            ptfe_measurement_tool();
        }
    }
    else if (part == "ptfe-measurement-tool-endcap-a") {
        if ($preview) {
            ptfe_measurement_tool_endcap_a();
        } else {
            ptfe_measurement_tool_endcap_a();
        }
    }
    else if (part == "ptfe-measurement-tool-endcap-b") {
        if ($preview) {
            ptfe_measurement_tool_endcap_b();
            preview_ptfe_measurement_tool_endcap_b();
        } else {
            ptfe_measurement_tool_endcap_b();
        }
    }
    else if (part == "ptfe-measurement-tool-stopper") {
        if ($preview) {
            ptfe_measurement_tool_stopper();
        } else {
            ptfe_measurement_tool_stopper();
        }
    }
    else if (part == "badge-holder") {
        if ($preview) {
            badge_holder();
            preview_badge_holder();
        } else {
            badge_holder();
        }
    }
    else if (part == "badge-holder-template") {
        if ($preview) {
            badge_holder_template();
            preview_badge_holder_template();
        } else {
            badge_holder_template();
        }
    }
    else if (part == "hygro-holder") {
        if ($preview) {
            hygro_holder();
            preview_hygro_holder();
        } else {
            hygro_holder();
        }
    }
    else if (part == "hygro-clip") {
        if ($preview) {
            hygro_clip();
            preview_hygro_clip();
        } else {
            hygro_clip();
        }
    }
    else if (part == "container-base") {
        if ($preview) {
            container_base();
        } else {
            rz(-90)
            container_base();
        }
    }
    else if (part == "container-base-preview") {
        if ($preview) {
            container_base_preview();
        } else {
            container_base_preview();
        }
    }
    else if (part == "container-base-test") {
        if ($preview) {
            container_base_test();
        } else {
            rz(-90)
            container_base_test();
        }
    }
    else if (part == "container-base-connector-a") {
        if ($preview) {
            container_base_connector_a();
        } else {
            container_base_connector_a();
        }
    }
    else if (part == "container-base-connector-b") {
        if ($preview) {
            container_base_connector_b();
        } else {
            container_base_connector_b();
        }
    }
    else if (part == "mount-rack-simple") {
        if ($preview) {
            mount_rack_simple();
        } else {
            rx(-90)
            mount_rack_simple();
        }
    }
    else if (part == "mount-rack-drill-template") {
        if ($preview) {
            mount_rack_drill_template();
        } else {
            mount_rack_drill_template();
        }
    }
    else {
        assert(false, str("invalid part ", part));
    }
}

// === automain end ===
