container_base_size = [100, 220, 18];
container_interface_yofs = 60;
container_rot = 8;
container_spc = 5;

container_base_xform_lf = tmat(x=-container_spc) * rzmat(container_rot/2, [0, -container_base_size.y/2, 0]);
container_base_xform_rt = tmat(x=container_spc) * rzmat(-container_rot/2, [0, -container_base_size.y/2, 0]);

module container_screw_cut() {
    size = container_base_size;
    tx(5)
    let(
        y1 = 7,
        y2 = 16.2,
        y3 = size.z + eps,
        d1 = 2.9,
        d2 = screw_diameter_m3 + .45,
        d3 = 5.8,
    )
    lathe(d=[
        [-eps, d1],
        [y1, d1],

        [y1, d2],
        [y2, d2],

        [y2, d3],
        [y3, d3]
    ]);
}

module container_interface(inner) {
    size = container_base_size;
    split_z = size.z - 4;

    fc = inner ? .25 : 0;
    xlen = 10 + fc;

    xzy() lxt(zc = 0, zs = 10 + 2*fc) {
        polyx([
            [-eps, -eps],
            [-eps, split_z],
            [0, split_z],
            [xlen, split_z - xlen * .7],
            [xlen, -eps],
        ]);
    }
}

module container_base(test=false, brim=true) {
    size = container_base_size;
    part_color(0) {
        difference() {
            cubex(
                xc = 0, xs = size.x,
                yc = 0, ys = size.y,
                z1 = 0, z2 = size.z,
                r = -15
            );

            cubex(xc=0, xs=container_width_bot, yc=0, ys=container_length_bot, r=-16.2/sqrt(2), z1 = 2, z2=size.z + eps);

            if (!test)
            tx(-size.x/2)
            ty([-container_interface_yofs, container_interface_yofs]) {
                container_interface(true);
                container_screw_cut();
            }
        }

        if (!test)
        tx(size.x/2)
        ty([-container_interface_yofs, container_interface_yofs])  difference() {
            container_interface(false);
            container_screw_cut();
        }
    }

    part_color(1)
    if (brim) rfy() {
        rfx()
        tx(size.x/2)
        ty(size.y/2 - 20)
        rz(-90)
        brim_tab(l=2, w=6, d=16);

        tx(-size.x/2)
        ty([container_interface_yofs - 10, container_interface_yofs + 10]) {
            rz(90)
            brim_tab(l=2, w=6, d=16);
        }

        tx(size.x/2 + 10)
        ty(container_interface_yofs)
        rz(-90)
        brim_tab(l=2, w=6, d=16);

    }
}

module container_base_test() {
    zslice(zc=container_base_size.z - 1, zs=1)
    container_base(test=true);
}


module container_base_preview() {
    size = container_base_size;
    xform_rt_a = tmat(x=size.x/2) * invert_mat(container_base_xform_lf);
    xform_rt_b = container_base_xform_rt * tmat(x=size.x/2);
    xform_lf_a = tmat(x=-size.x/2) * invert_mat(container_base_xform_rt);
    xform_lf_b = container_base_xform_lf * tmat(x=-size.x/2);

    multmatrix(xform_rt_a) {
        container_base_connector_a();
        container_base_connector_b();
        multmatrix(xform_rt_b) {
            multmatrix(xform_rt_a) {
                container_base_connector_a();
                container_base_connector_b();
                multmatrix(xform_rt_b) {
                    color("gold", 0.5)
                    render(10)
                    container_base();
                }
            }

            color("gold", 0.5)
            render(10)
            container_base();
        }
    }

    multmatrix(xform_lf_a) {
        container_base_connector_a();
        container_base_connector_b();
        multmatrix(xform_lf_b) {
            multmatrix(xform_lf_a) {
                container_base_connector_a();
                container_base_connector_b();
                multmatrix(xform_lf_b) {
                    color("gold", 0.5)
                    render(10)
                    container_base();
                }
            }

            color("gold", 0.5)
            render(10)
            container_base();
        }
    }

    color("gold", 0.5)
    render(10)
    container_base();

    *ty(-size.y/2) {
        tx(size.x/2 + container_spc)
        rz(-container_rot/2) {
            ty(size.y/2) {
                container_base_connector_a();
                container_base_connector_b();
            }
        }
        tx(-size.x/2 - container_spc)
        rz(container_rot/2) {
            ty(size.y/2) {
                container_base_connector_a();
                container_base_connector_b();
            }


            rz(container_rot/2)
            ty(size.y/2) tx(-size.x/2 - container_spc) color("gold", 0.5) render(10)
            container_base();
        }
    }


}

module container_base_connector(yofs) {
    size = container_base_size;
    rot = container_rot/2;


    base_pts = [
        [6, yofs + 8, 0],
        [0, yofs + 8, 0],
        [0, yofs - 8, 0],
        [6, yofs - 8, 0],
    ];


    poly_pts = [
        each xform_pts(container_base_xform_lf, base_pts),
        each reverse(xform_pts(container_base_xform_rt * tmat(xx=-1), base_pts))
    ];

    difference() {
        lxt(0, size.z) polyx([
            for (pt = poly_pts) vec2(pt)
        ]);
        multmatrix(container_base_xform_lf) ty(yofs) {
            container_interface(true);
            container_screw_cut();
        }
    }
    multmatrix(container_base_xform_rt) ty(yofs) difference() {
        container_interface(false);
        container_screw_cut();
    }


}

module container_base_connector_a() {
    container_base_connector(-container_interface_yofs);
}

module container_base_connector_b() {
    container_base_connector(container_interface_yofs);
}
