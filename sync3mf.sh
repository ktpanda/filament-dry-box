#!/bin/bash

pfx=roll-adapters-threaded
sfx=mm
sizes='50 52 54 56 58 60'
files=''
for j in $sizes; do
    files="$files $pfx-$j$sfx.3mf"
done

# 58mm = copper, black
# Amolen, Elegoo (plastic spools)

# 56mm = gold, black
# Paramount

# 54mm = pink, black
# Overture, Polymaker, Eryone, Elegoo (cardboard spools)

# 52mm = white, black
# Prusa Polymers Mini

# 50mm = black, white
# Prusament

newest=`ls -t $files | head -n 1`
repl=${newest%.3mf}
repl=${repl#$pfx-}

for j in $sizes; do
    f=$pfx-$j$sfx.3mf
    if [ "$f" != "$newest" ]; then
        ./ktpanda/replace_3mf_stl.py "$newest" -r $repl $j$sfx
    fi
done
