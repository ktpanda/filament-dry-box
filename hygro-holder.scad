
module hygro_clip_xform() {
    rx(90) tz(-hygro_clip_thick - hygro_yofs) children();
}

module hygro_holder() {
    //preview_slice("y", v1=10.9, v2=30)
    difference() {
        let(
            fw = 45, cw = 70,
            ys = hygro_yofs + hygro_clip_thick + 3,
            bevel = (cw - fw) / 2,
            z1 = -3, z2 = 1.4, z3 = hygro_holder_height
        )
        difference() {
            // outer
            lxt(z1, z3) squarex(xc = 0, xs=cw, y1=0, y2=ys, r1=-bevel, r2=-bevel);

            color("#b2ebc1") {
                lxt(z2, z3 + eps) {
                    // wide cut across front
                    squarex(xc = 0, xs=cw, y1=-eps, y2=hygro_yofs - 4.4);

                }

                // front cutout
                xzy() lxt(0, ys/2) {
                    let(b=5)
                    squarex(xc = 0, xs=hygro_clip_width - 10, y1=z2 + 3, y2=z3 + eps, r1=-b, r2=-b);
                }

                // rear cutout
                xzy() lxt(ys - 4, ys + 1) {
                    ty(hygro_holder_height/2)
                    let(qr=6)
                    for (xx = [-1 : .5 : 1])
                    tx(xx * (hygro_clip_width/2 - 2 - qr/2))
                    hull()
                    rfy()
                    ty(4.5)
                    rz(45)
                    square(qr/sqrt(2), center=true);
                }
            }

        }

        hygro_clip_xform()
        hygro_clip_base(true);
    }
}

module preview_hygro_holder() {
    tz(hygro_zofs)
    hygro_clip_xform() {
        color("#9db22e") render(10) hygro_clip();
        preview_hygro_clip();
    }
}

module round_poly(amt) {
    if (amt != 0) {
        offset(amt) offset(-amt) children();
    } else {
        children();
    }
}

module hygro_clip_base(inner) {
    xfc = inner ? .4 : 0;
    tfc = inner ? .2 : 0;

    lxt(-xfc, hygro_clip_thick + xfc) {
        rfx() {
            difference() {
                let (r = inner ? 0 : 1.5)
                squarex(
                    x1 = 0, x2 = hygro_clip_width/2 + tfc,
                    y1=0, y2=hygro_clip_height,
                    r4 = r
                );

                if (!inner)
                ty((hygro_clip_height - hygro_height) / 2) {
                    squarex(x1 = 0, x2 = hygro_width/2, y1=0, y2=hygro_height);


                    tx(hygro_width/2) {
                        //space for tabs
                        ty(hygro_height/2)
                        squarex(x1=0, x2=1.4, yc=0, ys=7.0);

                        // corner notches
                        rfy(hygro_height/2)
                        rz(45)
                        square(1, center=true);
                    }
                }
            }
        }
    }

    // bezel of hygrometer
    if (inner) {
        translate([0, hygro_clip_height/2, hygro_clip_thick])
        let(
            ofs=1.6,
            xs = hygro_clip_width - 2,
            ys = hygro_clip_height - 2
        )
        frustum_base(
            lz = 0,
            lx1 = -xs/2,
            ly1 = -ys/2,
            lx2 = xs/2,
            ly2 = ys/2,

            uz = 2.2,
            ux1 = -xs/2 + ofs,
            uy1 = -ys/2 + ofs,
            ux2 = xs/2 - ofs,
            uy2 = ys/2 - ofs,
        );

    }

    // side clips
    lxt(-xfc, 10 + xfc) rfx() {
        //round_poly(inner ? 0 : .4)
        ty(3)
        tx(hygro_clip_width / 2) {
            bow_clip_height = 19;
            bow_yofs = 0;
            bow_height = 23;
            bow_width = 3;

            // chord_dist() was incorrect, but I had already tweaked the values to fit
            // well and printed a bunch.

            //bow_xofs = chord_dist(bow_width, bow_height);

            bow_xofs = 21.875;

            bow_rad = bow_width + bow_xofs;
            bow_cpos = [-bow_xofs - 0.6, bow_height/2 + bow_yofs];

            // Rotates the bow itself but not the intersections
            bow_rot_inner = (inner ? 3 : -1);

            rz(($preview && !inner) ? hygro_yrot : 0)
            intersection() {
                difference($fn = $fn * 4) {
                    rz(bow_rot_inner)
                    translate(bow_cpos)
                    circle(r = bow_rad + 1.8 + xfc);

                    if (!inner)
                    difference() {
                        rz(bow_rot_inner)
                        translate(bow_cpos)
                        circle(r = bow_rad + .6);

                        // attachment of clip to main body
                        squarex(x1 = -1, x2 = bow_width, y1 = 0, y2 = 1.7);

                    }

                    // keep top of clip away from main body
                    if (!inner)
                    squarex(x1 = -1, x2 = 2.0, y1 = bow_height/2, y2 = bow_clip_height);
                }

                squarex(x1 = -1, x2 = bow_width + 3, y1 = 0, y2 = bow_clip_height);
            }

            // fill in the space cut by the main bevel
            //squarex(x1 = -2, x2 = 0, y1 = 0, y2 = 5);
        }
    }
}

module hygro_clip() {
    hygro_clip_base(false);
}

module preview_hygro_clip() {
    ty(hygro_clip_height / 2)
    tz(hygro_clip_thick + eps)
    hygro_model();
}
