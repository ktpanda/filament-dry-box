
module silica_clip() {
    size = [80, 26];
    tt = 3.6;

    difference() {
        cubex(
            xc=0, xs=size.x,
            yc=0, ys=size.y,
            z1=-tt, z2=0
        );

        rfx() tx(15) {
            tz(-tt+1.2) rz(30) hexnut_hole(h=5, d=nut_diameter_m3);
            cylx(-tt - eps, eps, d=screw_diameter_m3);
        }
    }

    for (i = [-1, 1]) sx(i)
    tx(size.x/2) {
        difference() {
            yzx()
            cubex(
                z1=0, z2=4,
                xc=0, xs=size.y,
                y1=-tt, y2=21,
                r3 = 2, r4 = 2
            );
            cubex(
                x1=-eps, x2=2,
                yc=0, ys=size.y - 4,
                z1=15.4, zs=2.6
            );

            tz(18.6)
            xzy() lxpoly(size.y + eps, center=true, points=[
                [-eps, 0],
                [4, 4],
                [-eps, 4],
            ]);

            if (i == 1) {
                xzy() lxpoly(size.y + eps, center=true, points=[
                    [-eps, 0],
                    [-eps, 15],
                    [2.4, 15-2.4],
                    [2.4, 0],
                ]);

            }
        }

    }
}

module preview_silica_clip() {
    *tz(3.4)
    color("green")
    import("origstl/silicabox-mountclipv3-ex.stl");
}

module silica_clip_template() {
    size = [115, 83.5];
    csize = [84, 26.4];

    h = 5;

    wt = 2;

    irad = 6.2;

    x1 = -24;
    x2 = 115 / 2 - 11;

    difference() {
        cubex(
            x1 = x1, x2 = x2 + wt,
            yc = 0, ys = size.y + 2*wt,
            z1 = -1.6, z2 = h,
            r2 = irad + wt, r4 = irad + wt
        );
        cubex(
            x1 = x1-eps, x2 = x2,
            yc = 0, ys = size.y,
            z1 = 0, z2 = h + eps,
            r2 = irad, r4 = irad
        );

        rfx()
        tx(15) {
            cylx(-2 - eps, 5+eps, d=screw_diameter_m3);
        }

        let(i=6)
        rfy() {
            cubex(
                x1 = x1 + i, x2 = x2 - i,
                y1 = i, y2 = size.y/2 - i,
                z1 = -2 - eps, z2 = eps, r=3
            );
        }
    }


    *difference() {
        cubex(
            x1=-10, x2=size.x/2,
            yc=0, ys=size.y,
            z1 = 0, z2 = 1.2,
            r = 16
        );

        tx(11)
        rfx() tx(15) {
            cylx(-eps, 5+eps, d=screw_diameter_m3);
        }
        *tx(size.x/2 - 8.5) cylx(-eps, 1.4 + eps, d=8);
        rfy() {
            cubex(
                x1=0, x2=size.x/2 - 10,
                y1 = 5, y2=size.y/2 - 10,
                z1 = -eps, z2 = 1.4 + eps,
                r3 = 6, r4 = 6
            );

        }
    }
}

module preview_silica_clip_template() {
    tz(3.6)
    //ry(180)
    silica_clip();
}

module silica_funnel() {
    difference() {
        union() {
            cylx(0, funnel_height, d1=funnel_outer_dia, d2=funnel_upper_outer_dia);
            cylx(-funnel_tube_length, eps, d=funnel_outer_dia);

            tz(funnel_height)
            cubex(
                x1=0, x2=funnel_upper_outer_dia/2 + funnel_handle_length,
                yc=0, ys=funnel_handle_width,
                z1=-funnel_handle_thick, z2=0,
                r=funnel_handle_width/2
            );
        }
        cylx(0, funnel_height+eps, d1=funnel_inner_dia, d2=funnel_upper_inner_dia);
        cylx(-funnel_tube_length-eps, eps, d=funnel_inner_dia);

        tz(funnel_height) tx(funnel_upper_outer_dia/2 + funnel_handle_length - funnel_handle_width/2)
        cylx(-funnel_handle_thick-eps, eps, d=6);
    }
}

function funnelpts(ofs=0, xofs=0) = [
    [ofs + xofs, -ofs],
    [ofs + xofs, cookiesheet_funnel_dia + ofs],
    [-cookiesheet_funnel_dia/2 - ofs / sqrt(2), cookiesheet_funnel_dia + ofs],
    [-cookiesheet_funnel_dia - ofs * sqrt(2), cookiesheet_funnel_dia/2],

    [-cookiesheet_funnel_dia/2 - ofs / sqrt(2), -ofs],
];



module cookiesheet_silica_funnel() {
    width = cookiesheet_funnel_size.x;
    length = cookiesheet_funnel_size.y;
    height = cookiesheet_funnel_size.z;
    csheight = 16;
    wt = cookiesheet_wall_thick;

    difference() {
        union() {
            cubex(
                xc = 0, xs = width + 2*wt,
                y1 = -wt, y2 = length,
                z1 = -wt, z2 = height + wt
            );

            tx(width/2)
            xzy() tz(-cookiesheet_funnel_len - wt) lxpoly(cookiesheet_funnel_len + wt, points=funnelpts(wt));

        }

        cubex(
            xc = 0, xs = width,
            y1 = 0, y2 = length + eps,
            z1 = 0, z2 = height
        );

        tx(width/2) {
            xzy() tz(-cookiesheet_funnel_len - wt - eps) lxpoly(cookiesheet_funnel_len + wt + eps2, points=funnelpts(0));
            cubex(
                x1 = -cookiesheet_funnel_dia - wt, x2 = wt + eps,
                yc = -wt/2, ys = 1.2,
                z1 = -wt/2, z2 = cookiesheet_funnel_dia + wt/2
            );
        }

        rfy(length/2 - wt/2) {
            tz([-wt/2, height + wt/2])
            translate([3, -wt - 1])
            xzy() {
                countersink_m2(wt*2 + 1, inv=1);
                cylx(0, 20, d=2.3);
            }
        }
    }


    *difference() {
        linear_extrude(height) polygon(funnelpts(length));

        tz(4)
        linear_extrude(height - 8) offset(-4) polygon(funnelpts(length));

        tz(4)
        linear_extrude(csheight) offset(-4) polygon(funnelpts(length + 20));

        tz(-eps)
        translate([-1,1] * 10)
        translate([1,1] * 20)
        linear_extrude(4 + eps2) polygon(funnelpts(length + 20));

    }
}

module cookiesheet_silica_funnel_slide() {
    wt = cookiesheet_wall_thick;
    y1 = -wt/2 + .3;
    y2 = cookiesheet_funnel_dia + wt/2 - .3;
    cubex(
        x1 = -cookiesheet_funnel_dia - wt, x2 = wt + eps,
        z1 = 0, z2 = 0.8,
        y1 = y1, y2 = y2
    );

    ofs = .4;

    cubex(
        x1 = wt/2, x2 = 10,
        z1 = 0, z2 = 4,
        y1 = y1 - ofs, y2 = y2 + ofs
    );

    linear_extrude(0.8) polygon([
        [0, y1],
        [wt/2, y1 - ofs],
        [10, y1 - ofs],
        [10, y2 + ofs],
        [wt/2, y2 + ofs],
        [0, y2],
    ]);

}

module preview_cookiesheet_silica_funnel() {
    wt = cookiesheet_wall_thick;
    tx(cookiesheet_funnel_size.x/2)
    ty(-wt/2 + .4)
    rx(90)
    render(10) cookiesheet_silica_funnel_slide();
}

module cookiesheet_silica_funnel_interface_h(fc=0) {
    wt = cookiesheet_wall_thick;

    cubex(x1 = -cookiesheet_funnel_size.x/2 - wt - eps, x2=0,
        y1 = -20, y2 = cookiesheet_funnel_size.y + wt,
        z1 = -wt - eps, z2 = cookiesheet_funnel_size.z + wt + eps
    );
    rfy(cookiesheet_funnel_size.y/2 - wt/2) {
        cubex(
            x1 = -eps, x2 = 6,
            y1 = -wt - fc - eps, y2 = wt + fc + eps,
            z1 = -wt - eps, z2 = cookiesheet_funnel_size.z + wt + eps,
        );
    }
}

module cookiesheet_silica_funnel_r() {
    intersection() {
        cookiesheet_silica_funnel();
        cookiesheet_silica_funnel_interface_h(fc=0);
    }
}

module cookiesheet_silica_funnel_l() {
    difference() {
        cookiesheet_silica_funnel();
        cookiesheet_silica_funnel_interface_h(fc=.3);
    }
}

module silica_box_slits(wt, length, y1, y2, spacing = 4) {

    lxt(-eps, wt + eps) {
        for (xx = open_range(5, length - 5, base=length/2, stepn=spacing)) tx(xx) {
            squarex(xc = 0, xs = 1.4, y1 = y1, y2 = y2, r=-.7);
        }
    }
}

module silica_box_zslits(inset=4, yofs=0) {
    size = silica_box_outer_size;
    for (
        yy = open_range(inset, size.y - inset, stepn = 6, base = size.y/2 + yofs),
        xx = half_open_range_a(4, size.x - 22 - 4, stepn = 24),
    )
    translate([xx, yy]) {
        squarex(x1 = 0, xs = 22, yc=0, ys=1.4);

    }

}

module silica_box_lid_groove(pts, xofs) {
    size = silica_box_outer_size;
    wt = silica_box_wall_thick;

    x1 = -1;
    xe = size.x - wt;

    x2 = xofs;
    x3 = x2 + 5;

    path = create_path([
        path_straight(x2 - x1),
        path_skew(x3 - x2, s = -.15),
        path_straight(xe - x3),

    ]);

    path_extrude(pts, path);
}

function lid_groove_pts(fc, xo) = let(
    lt = silica_box_lid_thick,
    xh = 0.6 - fc * (sqrt(2) - 1),
    x2 = -fc,
    x1 = path_debug ? x2 : -fc - lt + xh
) [
    [xo + x1, lt],
    [xo + x2, xh],
    [xo + x2, 0],
];


module silica_box() {

    size = silica_box_outer_size;
    wt = silica_box_wall_thick;
    bt = silica_box_bottom_thick;
    lt = silica_box_lid_thick;


    difference() {
        //main box with everything; outer difference is for lid groove cut
        union() {
            difference() {
                cubex(
                    x1 = 0, x2 = size.x,
                    y1 = 0, y2 = size.y,
                    z1 = 0, z2 = size.z,
                    r = 1
                );

                // inner space
                cubex(
                    x1 = wt, x2 = size.x - wt,
                    y1 = wt, y2 = size.y - wt,
                    z1 = bt, z2 = size.z + eps,
                    r2 = 1, r4 = 1
                );

                // lid cut
                cubex(
                    x1 = -eps, x2 = wt + eps,
                    y1 = wt, y2 = size.y - wt,
                    z1 = size.z - lt, z2 = size.z + eps,
                );


                rfy(size.y/2) xzy() silica_box_slits(wt, size.x, 2.5, size.z - 4, spacing = 8);
                rfx(size.x/2) yzx() silica_box_slits(wt, size.y, 2.5, size.z - 4, spacing = 8);

                lxt(-eps, bt + eps)
                silica_box_zslits();
            }

            inset = silica_box_lid_inset;

            tz(size.z - lt) {
                //
                rfx(size.x/2) tx(wt) xzy() {
                    lxt(wt, size.y - wt) {
                        polyx([
                            [0, -lt],
                            [lt, 0],
                            [0, 0]
                        ]);
                    }

                }
                rfy(size.y/2) yzx() {
                    color("#e638ef")
                    tx(wt)
                    lxt(wt, size.x - wt) {
                        polyx([
                            [0, -lt],
                            [lt, 0],
                            [lt, lt],
                            [0, lt]
                        ]);
                    }


                    // outer groove interface for funnel
                    let(gt=1.8)
                    ty(lt)
                    lxt(.6, size.x - wt - 2) {
                        polyx([
                            [wt, -gt - wt],
                            [-gt, 0],
                            [wt, 0]
                        ]);
                    }
                }
            }
        }

        // lid groove cut
        path_pts = [
            [-lt - 5, 0],
            [-lt - 5, lt],
            each lid_groove_pts(0, 0),

            //         [0, 0],
            // [-lt - 5, 0],
            // [-lt - 5, lt],
            // [path_debug ? 0 : -lt, lt + (path_debug ? eps : 0)]
        ];

        //!lxtd(0,1) polyx(path_pts);

        tx(-1)
        tz(size.z - lt)
        rfy(size.y/2) ty(wt) rz(-90) {
            cubex(x1 = -lt, x2 = 0, y1 = -eps, y2 = wt + 6, z1 = 0, z2 = lt + eps);
            silica_box_lid_groove(path_pts, size.x - wt - 10);
        }

        tz(size.z)
        let(w = .8, c=2.6, ofs = chord_dist(w, c*2))
        lxt(-lt, eps) rfy(size.y/2) ty(wt + .65) intersection() {
            tx(c) ty(ofs) circle(r=w + ofs, $fn=100);
            squarex(x1 = 0, x2 = 2*c, y1=-w, y2=0);

        }


    }

    // tabs for clips
    ty(size.y/2)
    rfx(size.x/2)
    cubex(
        x1 = -1.6, x2 = 0,
        yc = 0, ys = 20,
        z1 = 0, z2 = 2
    );
}

module preview_silica_box() {
    *color("#f8b815", 0.2)
    import("silicaboxv3-ex.stl");

    *tz(17.75)
    translate(vec3(silica_box_outer_size)/2)
    ry(180)
    render(10) silica_clip();

    tx(-lid_slide)
    tz(silica_box_outer_size.z - silica_box_lid_thick)
    color("#05a00d")
    render(10) silica_box_lid();
}

function path_skew(f, s, slices=1) = let(length = hypot(f, s)) (
    slices == 1 ?
    [[tmat(y=f, x=s), length]] :
    [for (i = [1 : slices]) [tmat(y=f * i / slices, x=s * i / slices), length * i / slices] ]
);

module silica_box_lid() {
    fc = 0.15;
    size = silica_box_outer_size;
    wt = silica_box_wall_thick;
    lt = silica_box_lid_thick;

    y1 = wt + fc;
    y2 = size.y - wt - fc;
    y3 = size.y/2 - wt;

    x1 = wt + 4.8;
    x4 = size.x - wt;

    x2 = x4 - 6;
    x3 = x4 - 1;


    path_pts = [
        [-1, 0],
        [-1, lt],
        each lid_groove_pts(fc, y3)
    ];

    //!lxtd(0,1) polyx(path_pts);

    difference() {
        intersection() {
            union() {
                yzx() lxt(0, x1) {
                    squarex(x1 = y1, x2 = y2, y1 = 0, y2 = lt);
                }

                tx(-1) rz(-90) tx(-size.y/2) rfx()
                silica_box_lid_groove(path_pts, x1 + 5);

            }

            //lxt(0, lt) squarex(x1 = 0, x2 = size.x - wt, y1 = y1 + fc, y2 = y2 - fc, r2 = 1, r4 = 1);

            let(x2=size.x - wt + .4)
            xzy() lxt(0, size.y) polyx([
                [0, 0],
                [0, lt],
                [x2 - lt, lt],
                [x2, 0]
            ]);
        }

        // cut for snap tab
        rfy(size.y/2) ty(4.4) lxt(-eps, lt + eps) {
            squarex(x1=-eps, x2=8, yc=0, ys=1.3);
        }

        lxt(-eps, lt + eps)
        silica_box_zslits(4, 3);
    }

    let(w = 1.0, c=2.6, ofs = chord_dist(w, c*2))
    lxt(0, lt) rfy(size.y/2) ty(wt + fc) intersection() {
        tx(c) ty(ofs) circle(r=w + ofs, $fn=100);
        squarex(x1 = 0, x2 = 2*c, y1=-w, y2=0);

    }

    lxt(0, lt + 1.4) {
        let(
            r1 = 9,
            rs = 1,
            ofs = [-r1, 0],
            angofs = 30,
            pts = [ for (ang = closed_range(angofs, 90, stepn=360, stepd=$fn)) rotpt([0, -1], -ang)]
        )
        ty(size.y/2)
        rfy()
        ty(16 + r1)
        rz(angofs)
        polyx([
            each (r1 - rs) * pts,
            each [ for (ang = open_range(0, 180, stepn=360*2, stepd=$fn)) ofs + rotpt([1, 0], ang) ],
            each reverse((r1 + rs) * pts),

        ]);
        ty(size.y/2)
        rfy()
        ty(16)
        squarex(x1=0, x2=1.5, yc=0, ys=2);
    }


}

module silica_box_lid_combo(n=silica_box_combo_count) {
    size = silica_box_outer_size;
    wt = silica_box_wall_thick;

    xspc = 6;

    lid_yofs = -10;

    brim_pts = [
        [0, 5],
        [0, size.x/2],
        [0, size.x - 5],
        [wt + .2, lid_yofs - 9],
        [wt + .2 + .15, lid_yofs - size.x/2],
        [wt + .2 + .15, lid_yofs - size.x + 5],
    ];

    for (i = [0 : n-1]) tx((size.y + xspc) * i){
        part_color(0)
        rz(90) silica_box();

        part_color(0)
        tx(-size.y)
        ty(lid_yofs)
        rz(-90) silica_box_lid();

        part_color(1, "green") {
            lxt(-eps, 0.5) {
                rfx(-size.y/2)
                tx(-7) squarex(x1=0, x2=5, y1 = -10, y2 = 0);

                tx(-size.y/2)
                squarex(xc=0, xs=5, y1 = -10, y2 = -1.6);

                if (i != 0) {
                    tx(-size.y)
                    for (pt = brim_pts) translate(pt) {
                        squarex(x1=-2 * pt.x - xspc, x2=0, yc=0, ys=5);
                    }
                }
            }

            rfx(-size.y/2) tx(-7)
            rfy2(lid_yofs - size.x + wt, size.x)
            rz(180)
            brim_tab(l=1, d=10, w=5, h=.5);
        }
    }

    part_color(1, "green") {
        rfx2(-size.y, (size.y + 6) * (n - 1))
        for (pt = brim_pts) translate(pt) {
            rz(90)
            brim_tab(l=4, d=16, w=5, h=.5);
        }
    }
}

// Project the given normal vector onto a unit superellipse with the given power and its inverse
function superel_project_vector(normvec, se_pwr, se_ipwr, xs=1, ys=1) = (
    let(
        // Take the absolute value of the input vector so it's in the first quadrant
        a0 = [abs(normvec.x) / xs, abs(normvec.y) / ys],
    )
    // If se_ipwr is 0, project to a square
    se_ipwr < 1e-10 ? (
        let(
            v1 = a0.x > a0.y ? [xs, ys * a0.y / a0.x] : [xs * a0.x / a0.y, ys]
        )
        [normvec.x < 0 ? -v1.x : v1.x, normvec.y < 0 ? -v1.y : v1.y]
    ) :

    // Otherwise, compute the radius and multiply it by the normal vector
    normvec * ((a0.x ^ se_pwr + a0.y ^ se_pwr) ^ -se_ipwr)
);

// Defines the side profile of the side of the funnel
function funnel_curve(t) = bezierc_v(
    [ [0, 0], [.5, .4] ],
    [ [1, 1], [0, .4] ],
    t
);

// Convert the list of angles into a list of [cos, sin] vectors.
function funnel_angvec(isize) = let(
    // Generate a list of angles for the first quadrant.
    // Make sure that the angle to the corner of the rectangle is included in the list.
    corner_angle = atan2(isize.y, isize.x),
    angles = [
        each half_open_range_a(0, corner_angle, base=0, stepn=90, stepd=$fn),
        each half_open_range_a(corner_angle, 90, base=0, stepn=90, stepd=$fn),
    ],
) [for (ang = reverse(angles)) [cos(ang), sin(ang)]];

function extend_slice(slice, scl, z) = [for (pt = slice) vec3(pt * scl, pt.z + z)];

function funnel_slice_rot_xform(t, z0) = rymat(lerp(-20, 0, t), ofs=[10, 0, lerp(z0, 0, t)]);
//function funnel_slice_rot_xform(t) = ident_mat();

function funnel_slice(angle_normvec, xs, ys, z, roundness, cystretch, xform) =
let(
    // Superellipse parameters: (1 - x ^ se_pwr) ^ (1 / se_pwr)
    // Calculate both the power and the inverse. When roundness is 1, then
    // se_pwr is 2 and se_ipwr is 0.5, and it defines a circle: sqrt(1 - x^2).
    // As roundness approaches 0, se_pwr approaches infinity and the curve
    // approaches a square. The superel_project_vector function handles this as a
    // special case.
    se_pwr = 2 / roundness,
    se_ipwr = roundness / 2,

    // Calculate the points for the first quadrant.
    quad_xy = [
        for (angv = angle_normvec)
        superel_project_vector(angv, se_pwr, se_ipwr, xs, ys)
    ],

    // Mirror the points into the second quadrant.
    xy0 = [
        each [for (pt = reverse(quad_xy)) [-pt.x, pt.y]],
        each quad_xy,
    ],

    // Add the points below the x axis.
    xy1 = [
        each -1 * xy0,
        each xy0,

    ],

    // Apply ystretch. Scale each point's y coordinate based on x.
    xy2 = cystretch == 1 ? xy1 : [
        for (pt = xy1)
        [pt.x, pt.y * lerp(1, cystretch, (-pt.x / xs + 1) / 2)]
    ],

) xform_pts(xform, xypoints(xy2, 0));

function funnel_curve_params(isize, rad, zscl = 30, ystretch=1, z0=0) = [
    for (t = closed_range(0, 1, stepd=(highres ? 100 : 10)))
    let(
        // Get the [x, z] point for the current layer. Ranges from [0, 0] to [1, 1].
        cdata = funnel_curve(t),

        // Actual z coordinate for this slice.
        z = zscl * cdata.y,

        // Become more round as z increases. Could raise cdata.y to a power to implement
        // a gamma curve but linear looks fine.
        roundness = cdata.y,

        // Axis scale for superellipse
        xs = lerp(isize.x, rad, cdata.x),
        ys = lerp(isize.y, rad, cdata.x),

        // ystretch defines how much to stretch the left side of the rectangle to make
        // it a trapezoid. Don't apply it to the round end - scale it down to 1 based
        // on roundness.
        cystretch = lerp(ystretch, 1, roundness),

        xform = tmat(z=z) * funnel_slice_rot_xform(t, z0),
    )
    [xs, ys, z, roundness, cystretch, xform]
];


function funnel_slices(isize, rad, zscl = 30, lext=0, uext=0, ystretch=1, z0=0) = let(
    angle_normvec = funnel_angvec(isize),

    slices = [
        for (data = funnel_curve_params(isize, rad, zscl, ystretch, z0))
        funnel_slice(angle_normvec, data[0], data[1], data[2], data[3], data[4], data[5])
    ],

    // Extend the top slice up and the bottom slice down if lext or uext were specified.
    xslices = [
        each lext > 0 ? [extend_slice(slices[0], 1, -lext)] : [],
        each slices,
        each uext > 0 ? [extend_slice(at(slices, -1), 1, uext)] : [],
    ]
) xslices;

module silica_box_funnel(outer_rad=undef, inner_rad=undef, brim=true) {
    fc = .2;

    height = 56;
    uext = 16;

    // space below the funnel to top of box
    zsize1 = 2;

    // lip around box
    zsize2 = 5;

    outer_rad = !is_undef(outer_rad) ? outer_rad : (max(silica_box_funnel_dia, 10) / 2);
    inner_rad = !is_undef(inner_rad) ? inner_rad : outer_rad - 2;

    inner_rad_2 = inner_rad + 1.8;
    inner_rad_3 = inner_rad + 2.9;

    echo(d1 = inner_rad * 2);
    echo(d2 = inner_rad_2 * 2);
    echo(d3 = inner_rad_3 * 2);

    lt = silica_box_lid_thick;

    bsize = silica_box_funnel_size / 2;
    isize = bsize - [1, 1] * silica_box_wall_thick - [0, silica_box_lid_thick];
    osize = bsize + [3.5, 4.5];
    front_ofs = 3;

    slices_inner = funnel_slices(isize, inner_rad, zscl=height - uext, lext=eps, uext=uext + eps);

    slices_outer = funnel_slices(osize, outer_rad, zscl=height - uext, lext=0, uext=uext - zsize1, ystretch=(osize.y + front_ofs)/osize.y, z0=-zsize1);

    xform = funnel_slice_rot_xform(0, 0);

    part_color(0)
    difference() {
        // Outer funnel body
        render(10)
        union() {

            tz(zsize1)
            slice_extrude(slices_outer);

            multmatrix(xform)
            lxt(-zsize2, zsize1) polyx([
                [-osize.x, -osize.y - front_ofs],
                [osize.x, -osize.y],
                [osize.x, osize.y],
                [-osize.x, osize.y + front_ofs],
            ]);
        }

        // Inner funnel
        slice_extrude(slices_inner);

        multmatrix(xform) {
            // Main space for the box
            cubex(
                x1=-bsize.x - 20, x2 = bsize.x,
                y1 = -bsize.y - fc, y2 = bsize.y + fc,
                z1 = -(zsize1 + zsize2) - eps, z2 = 0
            );

            // A little bit of extra space to prevent friction with the lid
            cubex(
                x1=-bsize.x - 20, x2 = -isize.x + 1,
                y1 = -isize.y + 1, y2 = isize.y - 1,
                z1 = -(zsize1 + zsize2) - eps, z2 = 0.4
            );

            // Cut which mates with the lip on the box
            path_pts = [
                [-10, 0],
                [lt, 0],
                [0, -lt],
                [0, -10],
                [-10, -10],
            ];

            path = create_path([

                path_straight(bsize.x + osize.x - 30),

                // Curve the cut out and also scale it up
                [
                    for (t = closed_range(0, 1, stepd=10))
                    let(
                        f = t*40, // forward translation
                        s = 6 * bezierc(0, 0, .3, 1, t), // side translation (skew)
                        scl = 1.5 ^ t // [x, z] scale factor
                    )
                    [tmat(y=f, x=s, xx=scl, zz=scl), f]
                ],

                // Extend a bit more to avoid coincident surfaces
                path_straight(1)
            ]);

            rfy() render(10) intersection() {
                // Don't let the cut get too close to the outer edge
                cubex(
                    x1 = -osize.x - 20, x2 = osize.x + 20,
                    y1 = 0, y2 = osize.y + front_ofs - 2,
                    z1 = -zsize2 - eps, z2 = 0
                );

                // The cut itself.
                tx(bsize.x) rz(90) tx(bsize.y + .3) path_extrude(path_pts, path);
            }
        }

        // Debug the cut polygon
        *lxtd(0, 0.1) polyx(path_pts);
    }


    part_color("brim", "yellow", flag=brim)
    if (brim) {
        tz(height)
        lxt(-.4) difference() {
            rz(lerpn(0, 360, 6, ob=true))
            ty(outer_rad - 1) brim_tab_2d(w=5, l=2, d=18);
            circle(r=outer_rad);
        }
    }
}

module preview_silica_box_funnel() {
    xform = funnel_slice_rot_xform(0, -2);

    if (preview_silica_box)
    multmatrix(xform)
    translate(vec3(-(2*silica_box_outer_size - silica_box_funnel_size)/2, -silica_box_outer_size.z)) {
        *color("#4b575a")
        render(10)
        silica_box();
        preview_silica_box();
    }

    // Debugging funnel shape math
    *tx(100) {
        *lxtd(0, 0.1) polyx([
            each [
                for (i = closed_range(0, 1, stepd=50))
                let(pt = funnel_curve(i))
                100 * [pt.x, pt.z]
            ],
            [100, -100],
            [0, -100]
        ]);

        let(
            roundness = debug_roundness,
            se_pwr = 2 / roundness,
            se_ipwr = roundness / 2,
            ang = 1,
            m = tan(ang),
            xs = 100,
            ys = 50,
            //xx = .9,
            //pt = superel_project_vector([cos(ang), sin(ang)], se_pwr, se_ipwr),
            //mm = _superel_d1(xx, se_pwr, se_ipwr, 0)
        ) {
            mp_ang = atan2(ys, xs);

            dpts = [
                for (ang = [
                    each closed_range(0, mp_ang, stepn=90, stepd=50, base=0),
                    each half_open_range_b(mp_ang, 90, stepn=90, stepd=50, base=0),
                ])
                let(
                    pt = superel_project_vector([cos(ang), sin(ang)], se_pwr, se_ipwr, 100, 50)
                )
                [pt.x, pt.y]
            ];
            //echo(pt);
            lxtd(0, 0.1) polyx([

                each dpts,
                //[100, -10],
                [0, 0]
            ]);

            *translate(100 * pt) {
                color("green") lxt(0, .3) circle(d=2);
                //color("red") lxt(0, .2) linehull([[1, mm] * -10, [1, mm] * 10]) circle(d=1);

            }
        }
    }
}

/*
slices_inner = let(
base_slices =
top = at(base_slices, -1),
scl1 = inner_rad_2 / inner_rad,
scl2 = inner_rad_3 / inner_rad,
scl3 = (inner_rad_3 + .8) / inner_rad,
z1 = height - 14,
z2 = z1 + 1,
z3 = height - 1
) [
each base_slices,
extend_slice(top, 1, z1 - 1.0),

extend_slice(top, scl1, z1),
extend_slice(top, scl1, z2),
extend_slice(top, scl2, z2 + 2),
extend_slice(top, scl2, z3 - 2),

extend_slice(top, scl3, z3),
extend_slice(top, scl3, height + eps)
];
*/

// d1 = 26.3
// d2 = 29.3
// d3 = 31.8
// h 14.5
// tp = 3.75
module silica_box_funnel_bottle() {
    height = 56;

    difference() {
        silica_box_funnel(outer_rad = 31.8 / 2 + 2, inner_rad = 26.0 / 2 - 3);
        tz(height) ry(180) rotate_extrude() {
            let(
                r1 = 25.7 / 2,
                r2 = 29.3 / 2,
                r3 = 31.8 / 2,

                h = 14
            )
            polyx([
                [r1 - 1, -eps],
                [r1 - 1, 0],
                [r1, 5],
                [r1, h],
                [(r1 + r2)/2, h + .8],
                [r2, h],
                [r2, h - 1],
                [r3, h - 2],
                [r3, 5],
                [r3 + .6, 0],
                [r3 + .6, -eps]
            ]);
        }
    }
}

module preview_silica_box_funnel_bottle() {
    preview_silica_box_funnel();

}
