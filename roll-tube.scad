


module tube_tool_interface(fc=0) {
    ww = 4.0 + 2*fc;
    dia = ww * 2/sqrt(3);
    intersection() {
        union() {
            cylx(0, 1, d1=dia + 1, d2 = dia, $fn=6);

            cylx(1, 8, d=dia, $fn=6);
            *cylx(9, 13, d1=dia, d2=dia-1, $fn=6);
        }
        *tz(9)
        let(d=ww)
        for (ang = [0, 60, 120]) rz(ang) {
            xzy()
            lxpoly(d*2, convexity=10, center=true, points=[
                [-dia/2, -12],
                [-dia/2, -4],
                [-.4 - fc, 0],
                [-.3 - fc, 4],
                [.3 + fc, 4],
                [.4 + fc, 0],
                [dia/2, -4],
                [dia/2, -12],
            ]);
        }
    }

    *intersection() {
        union() {
            cylx(0, 2, d1=6 + 2*fc, d2=4.5 + 2*fc);
            cylx(2, 5, d1=4.5 + 2*fc, d2=4 + 2*fc);
        }
        for (ang = [0 : 60 : 360]) rz(ang)
        xzy() lxpoly(5, convexity=10, points=[
            [-2 - fc, 0],
            [-.6 - fc, 2],
            [-.6 - fc, 3],
            [0, 4],
            [.6 + fc, 3],
            [.6 + fc, 2],
            [2 + fc, 0],
        ]);
    }

}

module tube_threaded() {
    inset = rollholder_thick + 4;
    difference() {
        union() {
            cylinder(h=rolltube_length, d=rolltube_dia);
            tz(inset)
            screw_taper(rolltube_length - 2*inset, [1.6, 11.6, 10], taper_bot=1, taper_top=1);
        }

        *let(h=6, i=1.6)
        rfz(rolltube_length/2) tz(2) {
            for (ang = [0 : 60 : 360]) rz(ang)
            tx(rolltube_dia/2) xzy() lxpoly(rolltube_dia, center=true, points=[
                [0, 0],
                [-i, i],
                [-i, h-i],
                [0, h],
            ]);
        }

        rfz(rolltube_length/2)
        tz(-eps)
        render(10)
        tube_tool_interface(.2);
    }

}

module tube_support_xform() {
    for (x = [0 : tube_support_count_x - 1], y = [0 : tube_support_count_y - 1])
    translate([
        tube_support_spacing.x * x,
        tube_support_spacing.y * (y + (x % 2 == 1 ? .5 : 0))
    ])
    children();
}
module preview_tube_threaded() {
    *tz(-.2 - 15)
    tube_tool_interface();
}

module tube_support() {
    ext = 8;
    inset = rollholder_thick + 4;

    screw_def = [1.6, 11.6, 10];
    pitch = screw_def[0];


    support_pts = [
        for (target = [
            //rolltube_length * .2,
            rolltube_length * .35
        ])
        floor((target - inset) / pitch) * pitch + inset
    ];

    hh = at(support_pts, -1);

    lxt(0, hh) difference() {
        hull() tube_support_xform() {
            circle(d=18);
        }
        tube_support_xform() {
            circle(d=14);
        }
    }


    let(xo = 4.0, l = 14.5/2)
    tube_support_xform() {
        tz(support_pts)
        difference() {
            for (ang = half_open_range_b(0, 360, stepn=360, stepd=3))
            rz(ang - 50)
            tz(pitch * ((ang / 360) - 1))
            xzy() lxt(-1.5, 1.5) {
                polyx([
                    [l, 0],
                    [l, -l + xo],
                    [xo, 0]
                ]);
            }

            tz(-2 * pitch) screw_thread(screw_def, 3);
        }

        lxt(0, .6) difference() {
            for (ang = half_open_range_a(0, 360, stepn=360, stepd=3))
            rz(ang - 30)
            squarex(x1 = 0, x2 = l, yc=0, ys=3);

            circle(d=rolltube_dia);
        }
    }
}

module preview_tube_support() {
    *tube_support_xform() {
        color("green")
        tube_threaded();
    }
}
